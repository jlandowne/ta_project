/****************************************************************************
 Module
   SerialService.c

 Revision
   1.0.1

 Description
   This is a template file for implementing a simple service under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "SerialService.h"
#include "string.h"
#include "PacketInterpretation.h"
#include "ReceiveFSM.h"
/*----------------------------- Module Defines ----------------------------*/
#define RIP_MASK        (BIT0HI | BIT1HI | BIT2HI)
#define X_ADDR_MASK     (BIT0HI | BIT1HI | BIT2HI | BIT3HI)
#define Y_ADDR_MASK     (BIT4HI | BIT5HI | BIT6HI | BIT7HI)
#define TN_HEAD_MASK    (BIT8HI | BIT9HI | BIT10HI)
#define TN_MASK         (BIT11HI | BIT12HI | BIT13HI)

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
static int eventParamToTeamNumber (uint16_t eventParam);
static void eventParamToThreeParams (uint16_t eventParam, uint8_t *outArray);
static void eventParamToFourParams (uint16_t eventParam, uint8_t *outArray);
/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitSerialService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
****************************************************************************/
bool InitSerialService(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  /********************************************
   in here you write your initialization code
   *******************************************/
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostSerialService

 Parameters
     EF_Event_t ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostSerialService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunSerialService

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes

 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunSerialService(ES_Event_t ThisEvent)
{
  
  char StringPointer [5];
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  bool print = false;
  /********************************************
   TODO, SWAP PLACEHOLDER EVENTS TO THE ONES DEFINED AS BIBIT SAYS
   *******************************************/
  if (ThisEvent.EventType == DEAD_SUB) {
      //Start appending the string here
      int team = eventParamToTeamNumber(ThisEvent.EventParam);   //TODO GET TEAM NUMBER
      sprintf(StringPointer,"H%dXXXX",team);
      print = true;
  }
  else if (ThisEvent.EventType == DEAD_SUB) {
      uint8_t params [3];
      //Parse message:
      eventParamToThreeParams(ThisEvent.EventParam, params);
      sprintf(StringPointer,"H%dXXXX",params[2]);
      print = true;
  }
  else if (ThisEvent.EventType == DETONATION) {
      uint8_t params [3];
      //Parse message:
      eventParamToThreeParams(ThisEvent.EventParam, params);
      //Start appending the string here
      if (params[1] < 10 && params[0] < 10){
          //Case 1, both x and y are less than 10
          sprintf(StringPointer,"D0%d0%dX",params[0],params[1]);
      }
      else if (params[1] < 10 && params[0] >= 10) {
          //Case 2, x > 10, y less than 10
          sprintf(StringPointer,"L%d0%dX",params[0],params[1]);
      }
      else if (params[1] >= 10 && params[0] < 10) {
          //Case 2, y > 10, x less than 10
          sprintf(StringPointer,"L0%d%dX",params[0],params[1]);
      }
      else if (params[1] >= 10 && params[0] >= 10) {
          //Case 2, y > 10, and x > 10
          sprintf(StringPointer,"L%d%dX",params[0],params[1]);
      }
      print = true;
  }
  else if (ThisEvent.EventType == PING_OMNI) {
      sprintf(StringPointer,"PXXXXX");
      print = true;
  }
// **NOTE: right now, both ping types are labeled as P -- might want to
//         differentiate between omni pings and directed pings
  else if (ThisEvent.EventType == PING_DIRECTED) {
      sprintf(StringPointer,"PXXXXX");
      print = true;
  }
  
  else if (ThisEvent.EventType == LOCATION_UPDATE) {
      uint8_t params [4];
      //Parse message:
      eventParamToFourParams(ThisEvent.EventParam, params);
      
      StringPointer[1] = params[3];
      StringPointer[2] = params[0];
      StringPointer[2] = params[1];
      if (params[1] < 10 && params[0] < 10){
          //Case 1, both x and y are less than 10
          sprintf(StringPointer,"L%d0%d0%d",params[3],params[0],params[1]);
      }
      else if (params[1] < 10 && params[0] >= 10) {
          //Case 2, x > 10, y less than 10
          sprintf(StringPointer,"L%d%d0%d",params[3],params[0],params[1]);
      }
      else if (params[1] >= 10 && params[0] < 10) {
          //Case 2, y > 10, x less than 10
          sprintf(StringPointer,"L%d0%d%d",params[3],params[0],params[1]);
      }
      else if (params[1] >= 10 && params[0] >= 10) {
          //Case 2, y > 10, and x > 10
          sprintf(StringPointer,"L%d%d%d",params[3],params[0],params[1]);
      }
          
      
      print = true;
  }
  else if (ThisEvent.EventType == NEXT_TURN) {
      uint8_t turn = eventParamToTeamNumber(ThisEvent.EventParam);
      if (turn < 10) {
        sprintf(StringPointer,"N00%dXX",turn);
      }
      else if (turn >= 10 && turn < 100) {
        sprintf(StringPointer,"N0%dXX",turn);
      }
      else if (turn >= 100) {
        sprintf(StringPointer,"N%dXX",turn);
      }
      print = true;
  }
  
  //Print out the ascii string
  if (print) {
    printf("%s",StringPointer);
  }
  return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/
//When receiving a RIP function, the received message will receive only 1 value
static int eventParamToTeamNumber (uint16_t eventParam){
    return (eventParam) & RIP_MASK;
}

static void eventParamToThreeParams (uint16_t eventParam, uint8_t *outArray){
    //This is used to save into an array the incoming elements
    outArray[0] = (eventParam) & X_ADDR_MASK;
    outArray[1] = (eventParam & Y_ADDR_MASK) >> 4;
    outArray[2] = (eventParam & TN_HEAD_MASK) >> 8;
}

static void eventParamToFourParams (uint16_t eventParam, uint8_t *outArray){
    //This is used to save into an array the incoming elements
    outArray[0] = (eventParam) & X_ADDR_MASK;
    outArray[1] = (eventParam & Y_ADDR_MASK) >> 4;
    outArray[2] = (eventParam & TN_HEAD_MASK) >> 8;
    outArray[3] = (eventParam & TN_MASK) >> 11;
}
/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/


