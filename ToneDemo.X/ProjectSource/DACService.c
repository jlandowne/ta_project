/****************************************************************************
 Module
   TemplateService.c

 Revision
   1.0.1

 Description
   This is a template file for implementing a simple service under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
 ****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
 */
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_Port.h"
//#include <stdio.h>
#include "dbprintf.h"
#include "DACService.h"

/*----------------------------- Module Defines ----------------------------*/

#define TICKS_PER_SEC 100
#define TOGGLE_TIME (TICKS_PER_SEC / 2)


/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
 */

static void decodeTones(uint16_t sample);
static void resetDecoder(void);

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;

static uint8_t processing_step = 0;

// square-wave testing
static uint8_t PWMPeriodReg = 254;
static uint16_t PWMFreq = 0;
static bool PWMactive = false;


/****************************************************************************
 *                      BEGIN SCRIPT-GENERATED TABLES                       *
 ***************************************************************************/
// TMR0 ticks at Fosc/4 = 8MHz with 1:2 prescale to get 31us interrupt period
#define TONE_INT_PERIOD 123

// ADC interrupt scaling
// interruptPS is a postscaler of the same TMR0 interrupt as tone generation
// ADC_SCALE_MASK is a binary mask to count modulo 4
#define ADC_SCALE_MASK 0x03
static volatile uint8_t interruptPS = 1;
static volatile uint8_t decodeEnabled = 0;
// number of tones to generate
#define NUM_TONES 4

// number of samples for Goertzel decoding
#define NUM_SAMPLES 47


// GOERTZEL TYPES, CONSTANTS, AND WORKING VALUES

// Union typedefs to avoid bitshifts (compiler seems to be not that optimizing)
// Avoiding truncation requires a long multiply and divide.
// this struct allows access to middle two bytes of intermediate multiplication
// to avoid the use of multi-byte bitshifts/divisions
// PIC is little-endian, so LSBs come first followed by MSBs

struct ltype {
    int8_t trunc;
    int16_t intval;
    int8_t space;
};
// access middle two bytes directly to avoid division

typedef union RESULT_T {
    int32_t l;
    struct ltype hold;
} RESULT_T;
// Allows direct access to bytes of 16-bit data

typedef union INT16_T {
    int16_t i;
    int8_t b[2];
} INT16_T;
// Same as INT16_T but for 32-bit values

typedef union INT32_T {
    int32_t l;
    int16_t i[2];
} INT32_T;

// Intermediate result storage
static RESULT_T Qhold;
// Constants for filter propagation
static const int16_t C0 = 356;
static const int16_t C1 = 304;
static const int16_t C2 = 119;
static const int16_t C3 = -216;
// Storage for filter state
static INT16_T Q0[3];
static INT16_T Q1[3];
static INT16_T Q2[3];
static INT16_T Q3[3];
static int16_t M[NUM_TONES] = {0, 0, 0, 0,};

// Lookup tables for generating tones
// Tones are (Hz): 1000 1250 1.666670e+03 2500 
static const int8_t toneTable0[31] = {1, 2, 3, 4, 5, 6, 6, 6, 6, 6, 5, 4, 3, 2, 1, 0, -1, -2, -3, -4, -5, -5, -6, -6, -6, -6, -5, -4, -4, -3, -1,};
static const int8_t toneTable1[24] = {1, 3, 4, 5, 6, 6, 6, 6, 5, 4, 3, 1, 0, -2, -3, -4, -5, -6, -6, -6, -6, -5, -4, -3,};
static const int8_t toneTable2[18] = {2, 4, 5, 6, 6, 6, 5, 3, 1, -1, -2, -4, -5, -6, -6, -5, -4, -3,};
static const int8_t toneTable3[11] = {3, 5, 6, 6, 4, 1, -2, -4, -6, -6, -5,};

static volatile uint8_t toneIndex[NUM_TONES] = {0, 0, 0, 0,};
static const uint8_t toneLength[NUM_TONES] = {31, 24, 18, 11,};
static volatile uint8_t toneEnable[NUM_TONES] = {0, 0, 0, 0,};

// Storage for raw measurements from ADC before processing
static volatile uint8_t toneBuffer0[NUM_SAMPLES];
static volatile uint8_t toneBuffer1[NUM_SAMPLES];
static volatile uint8_t *readBuffer = &toneBuffer0;
static volatile uint8_t *writeBuffer = &toneBuffer1;
static volatile uint8_t writeIndex = 0;
static volatile uint8_t dataReady = 0;

/****************************************************************************
 *                       END SCRIPT-GENERATED TABLES                        *
 ***************************************************************************/

/*------------------------------ Module Code ------------------------------*/

/****************************************************************************
 Function
     InitTemplateService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
 ****************************************************************************/
bool InitDACService(uint8_t Priority) {
    ES_Event_t ThisEvent;

    MyPriority = Priority;

    /********************************************
     Initialize the ADC hardware to read RC3
     *******************************************/
    TRISCbits.TRISC3 = 1;
    ANSELCbits.ANSC3 = 1;
    ADCON1bits.ADFM = 0; // left-justified to gram just the 8 MSB
    ADCON1bits.ADCS = 0b010; // Fosc/32 clock; table 20-1, p271
    ADCON1bits.ADPREF = 0b00; // Vref = VDD
    ADCON0bits.CHS = 0b010011; // sec 20.4, p277
    ADCON0bits.ADON = 1;

    PIR1bits.ADIF = 0;
    PIE1bits.ADIE = 1;

    /********************************************
     Initialize the DAC hardware to output on RA2
     *******************************************/
    ANSELAbits.ANSA2 = 0; // DAC output

    DAC1CON0bits.PSS = 0; // Positive reference is VDD
    DAC1CON0bits.NSS = 0; // Negative reference is VSS
    DAC1CON0bits.OE1 = 1; // enable output on RA2
    DAC1CON0bits.EN = 1; // enable DAC output

    DAC1CON1bits.DAC1R = 16; // Initialize to 50% as test

    /********************************************
     Initialize debug on RA3,4,5
     *******************************************/
    ANSELA &= (BIT3LO & BIT4LO & BIT5LO);
    LATA &= (BIT3LO & BIT4LO & BIT5LO);
    TRISA &= (BIT3LO & BIT4LO & BIT5LO);

    /********************************************
     Initialize Timer0 for selected rate
     *******************************************/
    T0CON1bits.T0CS = 0b010; // Fosc/4 source
    T0CON1bits.T0CKPS = 0b0001; // 1:2 prescale
    TMR0H = TONE_INT_PERIOD;
    TMR0L = 0;
    T0CON0bits.T0EN = 1;

    PIE0bits.TMR0IE = 1;

    /********************************************
     Set up TMR2/CCP1 for PWM
     *******************************************/
    RC2PPS = 0x09; // CCP1
    TRISC2 = 1; // start off
    PR2 = PWMPeriodReg;
    CCP1CONbits.MODE = 0xF; // PWM mode
    CCP1CONbits.FMT = 1; // left-aligned period
    CCP1CONbits.EN = 1;
    CCPR1L = PWMPeriodReg << 6;
    CCPR1H = PWMPeriodReg >> 1;
    T2CONbits.CKPS = 0b111; // 128x prescale
    T2CLKCONbits.CS = 0b0001; // Fosc/4 source
    T2CONbits.ON = 1;

    /********************************************
     Enable peripheral and global interrupts
     *******************************************/
    INTCONbits.PEIE = 1;
    INTCONbits.GIE = 1;

    ES_Timer_InitTimer(DAC_TIMER, TOGGLE_TIME);

    printf("PerReg %u \n\r", PWMPeriodReg);
    PWMFreq = 62500ul / (uint16_t) PWMPeriodReg;
    printf("PWM Freq. %u \n\r", PWMFreq);

    // post the initial transition event
    ThisEvent.EventType = ES_INIT;
    if (ES_PostToService(MyPriority, ThisEvent) == true) {
        return true;
    } else {
        return false;
    }
}

/****************************************************************************
 Function
     PostTemplateService

 Parameters
     EF_Event_t ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
 ****************************************************************************/
bool PostDACService(ES_Event_t ThisEvent) {
    return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunTemplateService

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes

 Author
   J. Edward Carryer, 01/15/12, 15:23
 ****************************************************************************/
ES_Event_t RunDACService(ES_Event_t ThisEvent) {
    ES_Event_t ReturnEvent;
    ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

    switch (ThisEvent.EventType) {
        case ES_TIMEOUT:
            if (ThisEvent.EventParam == DAC_TIMER) {
                ;
            }
            break;

        case EV_PROCESS_TONES:
        {
            decodeTones(ThisEvent.EventParam);
        }
            break;

        case EV_DECODE_READY:
        {
            puts("Done decode. \n\r");
        }
            break;

        case EV_NEW_KEY:
        {
            switch (ThisEvent.EventParam) {
                case '1':
                {
                    if (toneEnable[0]) {
                        toneEnable[0] = 0;
                        puts("Tone 1 disabled\n\r");
                    } else {
                        toneEnable[0] = 1;
                        puts("Tone 1 enabled\n\r");
                    }
                }
                    break;

                case '2':
                {
                    if (toneEnable[1]) {
                        toneEnable[1] = 0;
                        puts("Tone 2 disabled\n\r");
                    } else {
                        toneEnable[1] = 1;
                        puts("Tone 2 enabled\n\r");
                    }
                }
                    break;

                case '3':
                {
                    if (toneEnable[2]) {
                        toneEnable[2] = 0;
                        puts("Tone 3 disabled\n\r");
                    } else {
                        toneEnable[2] = 1;
                        puts("Tone 3 enabled\n\r");
                    }
                }
                    break;

                case '4':
                {
                    if (toneEnable[3]) {
                        toneEnable[3] = 0;
                        puts("Tone 4 disabled\n\r");
                    } else {
                        toneEnable[3] = 1;
                        puts("Tone 4 enabled\n\r");
                    }
                }
                    break;

                case 'x':
                {
                    for (uint8_t i = 0; i < NUM_TONES; i++) {
                        toneEnable[i] = 0;
                    }
                    decodeEnabled = 0;
                    puts("All tones disabled\n\r");
                }
                    break;

                case 'a':
                {
                    decodeEnabled = 1;
                }
                    break;

                case 'd':
                {
                    ES_Event_t decodeEvent;
                    decodeEvent.EventType = EV_PROCESS_TONES;
                    decodeEvent.EventParam = 0;
                    PostDACService(decodeEvent);

                    puts("Decoding\n\r");
                }
                    break;

                case 'r':
                {
                    puts("Raw Data:\n\r    ");
                    for (uint8_t i = 0; i < NUM_SAMPLES; i++) {
                        printf("%d ", readBuffer[i]);
                    }
                    puts("\n\r");
                }
                    break;

                case 'p':
                {
                    puts("Detection:\n\r    ");
                    for (uint8_t i = 0; i < NUM_TONES; i++) {

                        printf("%u ", M[i]);
                    }
                    puts("\n\r");
                }
                    break;

                case ',':
                {
                    if (PWMPeriodReg < 255) {
                        PWMPeriodReg += 1;
                        printf("PWM per. %u ticks\n\r", PWMPeriodReg);
                        PWMFreq = 62500ul / (uint16_t) PWMPeriodReg;
                        printf("Sq Freq. %u Hz\n\r", PWMFreq);
                        PR2 = PWMPeriodReg;
                        CCPR1L = PWMPeriodReg << 6;
                        CCPR1H = PWMPeriodReg >> 1;
                    }
                }
                    break;
                case '<':
                {
                    if (PWMPeriodReg < 250) {
                        PWMPeriodReg += 5;
                        printf("PWM per. %u ticks\n\r", PWMPeriodReg);
                        PWMFreq = 62500ul / (uint16_t) PWMPeriodReg;
                        printf("Sq Freq. %u Hz\n\r", PWMFreq);
                        PR2 = PWMPeriodReg;
                        CCPR1L = PWMPeriodReg << 6;
                        CCPR1H = PWMPeriodReg >> 1;
                    }
                }
                    break;
                case '.':
                {
                    if (PWMPeriodReg > 12) {
                        PWMPeriodReg -= 1;
                        printf("PWM per. %u ticks\n\r", PWMPeriodReg);
                        PWMFreq = 62500ul / (uint16_t) PWMPeriodReg;
                        printf("Sq Freq. %u Hz\n\r", PWMFreq);
                        PR2 = PWMPeriodReg;
                        CCPR1L = PWMPeriodReg << 6;
                        CCPR1H = PWMPeriodReg >> 1;
                    }
                }
                    break;
                case '>':
                {
                    if (PWMPeriodReg > 17) {
                        PWMPeriodReg -= 5;
                        printf("PWM per. %u ticks\n\r", PWMPeriodReg);
                        PWMFreq = 62500ul / (uint16_t) PWMPeriodReg;
                        printf("Sq Freq. %u Hz\n\r", PWMFreq);
                        PR2 = PWMPeriodReg;
                        CCPR1L = PWMPeriodReg << 6;
                        CCPR1H = PWMPeriodReg >> 1;
                    }
                }
                    break;

                case 'l':
                {
                    PWMPeriodReg = 15;
                    printf("PWM per. %u ticks\n\r", PWMPeriodReg);
                    PWMFreq = 62500ul / (uint16_t) PWMPeriodReg;
                    printf("Sq Freq. %u Hz\n\r", PWMFreq);
                    PR2 = PWMPeriodReg;
                    CCPR1L = PWMPeriodReg << 6;
                    CCPR1H = PWMPeriodReg >> 1;
                }
                    break;

                case 'k':
                {
                    PWMPeriodReg = 255;
                    printf("PWM per. %u ticks\n\r", PWMPeriodReg);
                    PWMFreq = 62500ul / (uint16_t) PWMPeriodReg;
                    printf("Sq Freq. %u Hz\n\r", PWMFreq);
                    PR2 = PWMPeriodReg;
                    CCPR1L = PWMPeriodReg << 6;
                    CCPR1H = PWMPeriodReg >> 1;
                }
                    break;

                case 'm':
                {
                    if (PWMactive) {
                        PWMactive = false;
                        TRISC2 = 1;
                        printf("Sq. Tone Stop\n\r");
                    } else {
                        PWMactive = true;
                        TRISC2 = 0;
                        printf("Sq. Tone Start\n\r");
                    }
                }
                    break;
            }
        }
            break;
    }

    return ReturnEvent;
}

bool CheckForADCDone(void) {
    if (dataReady) // new key waiting?
    {
        dataReady = 0;

        ES_Event_t ThisEvent;

        ThisEvent.EventType = EV_PROCESS_TONES;
        ThisEvent.EventParam = 0;

        PostDACService(ThisEvent);

        return true;
    } else {
        return false;
    }
}

/****************************************************************************
 *                        SCRIPT-GENERATED FUNCTION                         *
 ***************************************************************************/
void ToneGenISR(void) {
    uint8_t output = 16;
    TMR0IF = 0;
    LATA3 = 1;

    // arbitrary modulo is expensive, so check for bound and wrap
    if (toneEnable[0]) {
        toneIndex[0] = (toneIndex[0] + 1);
        if (toneIndex[0] >= toneLength[0]) {
            toneIndex[0] = 0;
        }
        output += toneTable0[toneIndex[0]];
    }

    if (toneEnable[1]) {
        toneIndex[1] = (toneIndex[1] + 1);
        if (toneIndex[1] >= toneLength[1]) {
            toneIndex[1] = 0;
        }
        output += toneTable1[toneIndex[1]];
    }

    if (toneEnable[2]) {
        toneIndex[2] = (toneIndex[2] + 1);
        if (toneIndex[2] >= toneLength[2]) {
            toneIndex[2] = 0;
        }
        output += toneTable2[toneIndex[2]];
    }

    if (toneEnable[3]) {
        toneIndex[3] = (toneIndex[3] + 1);
        if (toneIndex[3] >= toneLength[3]) {
            toneIndex[3] = 0;
        }
        output += toneTable3[toneIndex[3]];
    }

    DAC1CON1bits.DAC1R = output & 0x1F; // explicitly constrain to 5 bits

    // Check if we need to start an ADC conversion
    interruptPS = (1 + interruptPS) & ADC_SCALE_MASK;
    if (interruptPS == 0 && decodeEnabled) {
        ADCON0bits.GO = 1;
    }
    LATA3 = 0;
}

/****************************************************************************
 *                        SCRIPT-GENERATED FUNCTION                         *
 ***************************************************************************/
void ToneDecodeISR(void) {
    ADIF = 0;
    LATA4 = 1;
    writeBuffer[writeIndex++] = ADRESH;
    if (writeIndex == NUM_SAMPLES) {
        if (writeBuffer == &toneBuffer0) {
            writeBuffer = &toneBuffer1;
            readBuffer = &toneBuffer0;
        } else {
            writeBuffer = &toneBuffer0;
            readBuffer = &toneBuffer1;
        }
        writeIndex = 0;
        decodeEnabled = 0;
        dataReady = 1;
    }
    LATA4 = 0;
}

/****************************************************************************
 *                        SCRIPT-GENERATED FUNCTION                         *
 ***************************************************************************/
static void decodeTones(uint16_t sample) {
    ES_Event_t nextEvent;
    int16_t x;
    if (sample < NUM_SAMPLES) {
        LATA5 = 1;
        x = readBuffer[sample];
        // Tone 0 Filter
        Qhold.l = (int32_t) C0 * (int32_t) Q0[1].i;
        Q0[0].i = x;
        Q0[0].i += Qhold.hold.intval;
        Q0[0].i -= Q0[2].i;
        Q0[2].i = Q0[1].i;
        Q0[1].i = Q0[0].i;
        // Tone 1 Filter
        Qhold.l = (int32_t) C1 * (int32_t) Q1[1].i;
        Q1[0].i = x;
        Q1[0].i += Qhold.hold.intval;
        Q1[0].i -= Q1[2].i;
        Q1[2].i = Q1[1].i;
        Q1[1].i = Q1[0].i;
        // Tone 2 Filter
        Qhold.l = (int32_t) C2 * (int32_t) Q2[1].i;
        Q2[0].i = x;
        Q2[0].i += Qhold.hold.intval;
        Q2[0].i -= Q2[2].i;
        Q2[2].i = Q2[1].i;
        Q2[1].i = Q2[0].i;
        // Tone 3 Filter
        Qhold.l = (int32_t) C3 * (int32_t) Q3[1].i;
        Q3[0].i = x;
        Q3[0].i += Qhold.hold.intval;
        Q3[0].i -= Q3[2].i;
        Q3[2].i = Q3[1].i;
        Q3[1].i = Q3[0].i;
        LATA5 = 0;
        nextEvent.EventType = EV_PROCESS_TONES;
        nextEvent.EventParam = sample + 1;
        PostDACService(nextEvent);
    } else {
        uint16_t bitTest = 0;
        uint8_t shift = 0;
        LATA5 = 1;

        // Find how many bits the computed Q require to represent
        if (Q0[1].i >= 0) bitTest |= Q0[1].i;
        else bitTest |= (~Q0[1].i) << 1;
        if (Q0[2].i >= 0) bitTest |= Q0[2].i;
        else bitTest |= (~Q0[2].i) << 1;
        if (Q1[1].i >= 0) bitTest |= Q1[1].i;
        else bitTest |= (~Q1[1].i) << 1;
        if (Q1[2].i >= 0) bitTest |= Q1[2].i;
        else bitTest |= (~Q1[2].i) << 1;
        if (Q2[1].i >= 0) bitTest |= Q2[1].i;
        else bitTest |= (~Q2[1].i) << 1;
        if (Q2[2].i >= 0) bitTest |= Q2[2].i;
        else bitTest |= (~Q2[2].i) << 1;
        if (Q3[1].i >= 0) bitTest |= Q3[1].i;
        else bitTest |= (~Q3[1].i) << 1;
        if (Q3[2].i >= 0) bitTest |= Q3[2].i;
        else bitTest |= (~Q3[2].i) << 1;

        // Compute shift from highest bit that is set
        if (bitTest & (1 << 15)) shift = 8;
        else if (bitTest & (1 << 14)) shift = 7;
        else if (bitTest & (1 << 13)) shift = 6;
        else if (bitTest & (1 << 12)) shift = 5;
        else if (bitTest & (1 << 11)) shift = 4;
        else if (bitTest & (1 << 10)) shift = 3;
        else if (bitTest & (1 << 9)) shift = 2;
        else if (bitTest & (1 << 8)) shift = 1;
        LATA5 = 0;
        LATA5 = 1;

        // Filter 0 Update
        Q0[1].i >>= shift;
        Q0[2].i >>= shift;
        // Filter 1 Update
        Q1[1].i >>= shift;
        Q1[2].i >>= shift;
        // Filter 2 Update
        Q2[1].i >>= shift;
        Q2[2].i >>= shift;
        // Filter 3 Update
        Q3[1].i >>= shift;
        Q3[2].i >>= shift;
        LATA5 = 0;
        LATA5 = 1;

        // Filter 0 magnitude calculation
        Qhold.l = (int32_t) Q0[1].i * (int32_t) Q0[2].i * (int32_t) C0;
        M[0] = ((Q0[1].i * Q0[1].i)
                +(Q0[2].i * Q0[2].i)
                -(Qhold.hold.intval));
        // Reset Filter 0
        Q0[1].i = 0;
        Q0[2].i = 0;
        // Filter 1 magnitude calculation
        Qhold.l = (int32_t) Q1[1].i * (int32_t) Q1[2].i * (int32_t) C1;
        M[1] = ((Q1[1].i * Q1[1].i)
                +(Q1[2].i * Q1[2].i)
                -(Qhold.hold.intval));
        // Reset Filter 1
        Q1[1].i = 0;
        Q1[2].i = 0;
        // Filter 2 magnitude calculation
        Qhold.l = (int32_t) Q2[1].i * (int32_t) Q2[2].i * (int32_t) C2;
        M[2] = ((Q2[1].i * Q2[1].i)
                +(Q2[2].i * Q2[2].i)
                -(Qhold.hold.intval));
        // Reset Filter 2
        Q2[1].i = 0;
        Q2[2].i = 0;
        // Filter 3 magnitude calculation
        Qhold.l = (int32_t) Q3[1].i * (int32_t) Q3[2].i * (int32_t) C3;
        M[3] = ((Q3[1].i * Q3[1].i)
                +(Q3[2].i * Q3[2].i)
                -(Qhold.hold.intval));
        // Reset Filter 3
        Q3[1].i = 0;
        Q3[2].i = 0;
        LATA5 = 0;

        nextEvent.EventType = EV_DECODE_READY;
        nextEvent.EventParam = 0;
        PostDACService(nextEvent);
    }
}

/***************************************************************************
 private functions
 ***************************************************************************/

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

