/****************************************************************************

  Header file for template Flat Sate Machine
  based on the Gen2 Events and Services Framework

 ****************************************************************************/

#ifndef ReceiveFSM_H
#define ReceiveFSM_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Port.h"
#include "ES_Events.h"

// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  InitPState, Waiting, Listening, Checksum, QuietTime
}TemplateState_t;

// Public Function Prototypes

bool InitReceiveFSM(uint8_t Priority);
REENTRANT bool PostReceiveFSM(ES_Event_t ThisEvent);
ES_Event_t RunReceiveFSM(ES_Event_t ThisEvent);
TemplateState_t QueryTemplateSM(void);
//Message getter function
uint32_t GetLatestMessage(void);
//Event Checker
bool CheckForLine(void);
//ISR Function
void RXISR (void);

#endif /* ReceiveFSM_H */

