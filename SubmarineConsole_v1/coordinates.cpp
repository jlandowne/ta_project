/*
 * file: coordinates.cpp
 * -----------------------
 * This class handles conversion from x,y coordinates to scene x,y pixels
 */

#include "coordinates.h"
#include "definitions.h"
#include <cstdlib>

#define HEX_Y_TRAVERSE 46
#define HEX_X_TRAVERSE 52
//origin is bottom most hexagon
#define Origin_X_HEX 7

int Coordinates::X_Pixels(int x, int y)
{
    int diff_x = (x - Origin_X_HEX)*HEX_X_TRAVERSE;
    return (ORIGIN_X + diff_x);
}

int Coordinates::Y_Pixels(int x, int y)
{
    int top_pix = ORIGIN_Y - 7*HEX_Y_TRAVERSE;
    int diff_x = abs(Origin_X_HEX - x);
    int diff_y;
    if(x <= 7) {
        diff_y = (14 - y);
    } else {
        diff_y = 14 - diff_x - y;
    }
    int posY_pix = top_pix + (HEX_Y_TRAVERSE*diff_x)/2 + (HEX_Y_TRAVERSE*diff_y);
    return posY_pix;
}
//V Holly
//int diff_x = x - Origin_X_HEX;
//int diff_y = -diff_x*HEX_TRAVERSE/2 - y*HEX_TRAVERSE;

//Justin V1
//int diff_y = (Origin_X_HEX - (7 - float(x))/2 - float(y))*HEX_Y_TRAVERSE;
//return(ORIGIN_Y+ diff_y);
