/*
 * file: enemy.cpp
 * ----------------------
 * This file creates enemies when we get position updates, and removes
 * enemies when we don't get updates.
 */

#include "enemy.h"
#include "definitions.h"
#include "game.h"
#include "qdebug.h"

#include <QGraphicsPixmapItem>

//height of enemy, specific to the image
#define h 50

extern Game * game;

Enemy::Enemy(QGraphicsItem *parent): QGraphicsPixmapItem(parent)
{
    recentUpdate = 3;
    coords = new Coordinates();
}

void Enemy::setEnemyNum(int num) {
    thisNum = num;
    QString imageLocation(":/images/sub");
    imageLocation.append(QString::number(thisNum));
    imageLocation.append(".png");
    QPixmap p = QPixmap(imageLocation);
    setPixmap(p.scaledToHeight(h));

    /* Testing purposes only!
     * places each enemy in a certain location to appear at start */
    curX = num + 2;
    curY = num + 5;
    int xLoc = coords->X_Pixels(curX, curY);
    int yLoc = coords->Y_Pixels(curX, curY);
    setPos(xLoc, yLoc);
}

void Enemy::updateEnemy(int xIndex, int yIndex, int lIndex)
{
    curX = xIndex;
    curY = yIndex;
    int xLoc_pix = coords->X_Pixels(xIndex, yIndex);
    int yLoc_pix = coords->Y_Pixels(xIndex, yIndex);
    if(lIndex == DEAD) {
        setPixmap(QPixmap(":/images/enemyred.png").scaledToHeight(h));
    }
    setPos(xLoc_pix, yLoc_pix);
    recentUpdate = 3;
    game->scene->update();

}

void Enemy::noUpdate()
{
    if(recentUpdate == 3) {
        setPixmap(QPixmap(":/images/enemygray.png").scaledToHeight(h));
        recentUpdate--;
    } else if (recentUpdate == 2) {
        recentUpdate--;
    } else if (recentUpdate == 1) {
        game->scene->removeItem(this);
        recentUpdate--;
    }
}


