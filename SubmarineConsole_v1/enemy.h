#ifndef ENEMY_H
#define ENEMY_H

#include <QObject>
#include <QGraphicsPixmapItem>
#include <coordinates.h>

class Enemy: public QObject, public QGraphicsPixmapItem {
    Q_OBJECT
public:
    Enemy(QGraphicsItem *parent = 0);
    void setEnemyNum(int num);
    void updateEnemy(int xIndex, int yIndex, int lIndex);
    void noUpdate();
    Coordinates * coords;
    int curX;
    int curY;


    int recentUpdate;

private:
    int thisNum;
};

#endif // ENEMY_H
