#ifndef GAME_H
#define GAME_H

#include <QGraphicsView>
#include <QWidget>
#include <QGraphicsScene>
#include <QTimer>
#include "sub.h"
#include "coordinates.h"
#include "gameaction.h"
#include <QSerialPort>
#include "button.h"
#include "enemy.h"
#include "definitions.h"
#include "explosion.h"

#define NumInGameStatus 20

class Game: public QGraphicsView {
    Q_OBJECT
public:
    Game(QWidget * parent=0);
    ~Game();
    void resetBoard() ;

    QGraphicsScene * scene;
    Coordinates * coord;
    GameAction * gameActionBox;
    QSerialPort * serial;
    QString commPortName = DEFAULT_SERIAL_PORT;
    QGraphicsTextItem * TurnText;
    QString * ComPort;
    Button * hitButton;
    Button * detonationButton;
    Button * pingButton;
    Explosion * explosion;

    uint8_t decodeCounter = 0;
    QByteArray * gamestatus;

    int numTeams = 8;


public slots:
    void serialReceived();
    void blinkHit();
    void blinkDetonation();
    void blinkPing();

private:
    QTimer * hitBlinkTimer;
    QTimer * detonationBlinkTimer;
    QTimer * pingBlinkTimer;
    void gotHit();
    void gotDetonation();
    void gotPing();
    QVector<Enemy *> teams;
    int numBadReads = 0;



};
#endif // GAME_H
