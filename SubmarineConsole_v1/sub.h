#ifndef SUB_H
#define SUB_H

#include <QObject>
#include <QGraphicsPixmapItem>
#include <QGraphicsItem>


class Sub: public QObject, public QGraphicsPixmapItem {
    Q_OBJECT
public:
    Sub(QGraphicsItem *parent = 0);
    void move();
    void turnLeft();
    void turnRight();
    void keyPressEvent(QKeyEvent * event);

    int Heading;
    int X_Coord;
    int Y_Coord;
};
#endif // SUB_H
