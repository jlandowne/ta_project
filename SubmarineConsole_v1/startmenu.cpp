/*
 * File: startmenu
 * ---------------
 * This file sets up the start menu for the game. When the start button is
 * pushed, this scene is hidden and the game scene is brought up
 */
#include "startmenu.h"
#include "button.h"
#include "game.h"
#include "definitions.h"
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QBrush>
#include <QLineEdit>
#include <QObject>
#include <QString>
#include <QComboBox>
#include <map>
#include <qdebug.h>

#define ITEM_START_POS_X 250
#define ITEM_START_POS_Y 250

extern Game * game;


static std::map<QString,int> headingmap = {{"UP", 90}, {"RIGHT UP", 150},
                                    {"RIGHT DOWN", 210}, {"DOWN",270},
                                    {"LEFT DOWN",330}, {"LEFT UP",30}};

StartMenu::StartMenu(QWidget *parent)
{
    /* First set up the scene with a fun background of a sub */
    scene = new QGraphicsScene();
    scene->setSceneRect(0,0,ScreenWidth*ScreenScale,ScreenHeight*ScreenScale);
    setBackgroundBrush(QBrush(QImage(":/images/startmenu.png")));
    setScene(scene);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setFixedSize(ScreenWidth*ScreenScale,ScreenHeight*ScreenScale);

    /* Create the instructions, input boxes, and buttons for the start up
     * sequence, in order as they go down the screen. */

    //Everything is centered around this x coordinate
    int txPos = ITEM_START_POS_X;
    int tyPos = ITEM_START_POS_Y;
    int itemNum = 0;

    //Set up com port input.
    CommPort = new QLineEdit();
    CommPort->setFixedHeight(STD_HEIGHT);
    CommPort->setFixedWidth(STD_WIDTH);
    QPoint p1(txPos - CommPort->width()/2, tyPos);
    CommPort->move(p1);
    scene->addWidget(CommPort);

    //Add directions for the com port
    QGraphicsTextItem* CommDir = new QGraphicsTextItem(QString("Input COM port full name"));
    QFont titleFont("courier", 14);
    CommDir->setDefaultTextColor(Qt::lightGray);
    CommDir->setFont(titleFont);
    CommDir->setPos(txPos-CommDir->boundingRect().width()/2, p1.y() + CommPort->height() + TEXT_OFFSET);
    scene->addItem(CommDir);
    itemNum++;

    //Add box for Number of teams
    NumTeams = new QLineEdit();
    NumTeams->setFixedHeight(50);
    NumTeams->setFixedWidth(100);
    tyPos += ITEM_OFFSET;
    QPoint p5(txPos - NumTeams->width()/2, tyPos);
    NumTeams->move(p5);
    scene->addWidget(NumTeams);

    //Add directions for nmber of teams
    QGraphicsTextItem* NumTeamsDir = new QGraphicsTextItem(QString("Input number of teams (0-7)"));
    NumTeamsDir->setDefaultTextColor(Qt::lightGray);
    NumTeamsDir->setFont(titleFont);
    NumTeamsDir->setPos(txPos-NumTeamsDir->boundingRect().width()/2, tyPos + NumTeams->height() + TEXT_OFFSET);
    scene->addItem(NumTeamsDir);
    itemNum++;

    //Add start button at bottom of screen
    StartButton = new Button(QString("Start Game"));
    tyPos += ITEM_OFFSET;
    StartButton->setPos(txPos - StartButton->boundingRect().width()/2, tyPos);
    StartButton->setColor(Qt::red);
    QObject::connect( StartButton, SIGNAL( clicked() ), this, SLOT(start_game())) ;
    scene->addItem(StartButton);


    this->show();
}

void StartMenu::start_game()
{
    scene->removeItem(StartButton);
    QString commport = (CommPort->text());
    if(!commport.isEmpty()) { //Will default default #define if empty
        if(!game->serial->isOpen()) {
            qDebug() << "Changing Port Name";
            game->commPortName = CommPort->text();
            game->serial->setPortName(game->commPortName);
            game->serial->open(QIODevice::ReadWrite);
            if(game->serial->isOpen()) {
               qDebug() << "Port Success!";
            } else {
               qDebug() << "PORT FAILURE, CANNOT CONNECT!!";
            }
        } else {
            qDebug() << "PORT FAILURE, CANNOT CONNECT!!";
        }
    }

    //Things to send to serial
    game->numTeams = NumTeams->text().toInt();

    this->hide();
    game->show();
}
