#ifndef DB_printf_H
#define DB_printf_H

int DB_printf(const char *Format, ...);
int DB_puts(const char *pString);

#define printf      DB_printf
#define puts        DB_puts
    
#endif