/*
 * file: enemy.cpp
 * ----------------------
 * This file creates enemies when we get position updates, and removes
 * enemies when we don't get updates.
 */

#include "explosion.h"
#include "definitions.h"
#include "game.h"
#include "qdebug.h"

#include <QGraphicsPixmapItem>

//height of enemy, specific to the image
#define h 50

extern Game * game;

Explosion::Explosion(QGraphicsItem *parent): QGraphicsPixmapItem(parent)
{
    coords = new Coordinates();
}

void Explosion::set(int x, int y) {
    QString imageLocation(":/images/explosion.png");
    QPixmap p = QPixmap(imageLocation);
    setPixmap(p.scaledToHeight(h));

    int xLoc = coords->X_Pixels(x, y);
    int yLoc = coords->Y_Pixels(x, y);
    qDebug() << xLoc << "  " << yLoc;
    setPos(xLoc, yLoc);
}

void Explosion::show()
{
    game->scene->addItem(this);
}

void Explosion::hide()
{
    game->scene->removeItem(this);
}


