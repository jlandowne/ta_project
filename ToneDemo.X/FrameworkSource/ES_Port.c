/****************************************************************************
 Module
   ES_Port.c

 Revision
   1.0.1

 Description
   This is the sample file to demonstrate adding the hardware specific
   functions to the Events & Services Framework. 
 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 04/18/19 10:17 jec     started work on port to PIC16F15356
 08/21/17 13:47 jec     added functions to init 2 lines for debugging the framework
                        and functions to set & clear those lines.
 03/13/14 10:30	joa		  Updated files to use with Cortex M4 processor core.
                        Specifically, this was tested on a TI TM4C123G mcu.
 03/05/14 13:20	joa		  Began port for TM4C123G
 08/13/13 12:42 jec     moved the hardware specific aspects of the timer here
 08/06/13 13:17 jec     Began moving the stuff from the V2 framework files
 ***************************************************************************/
// PIC16F15356 Configuration Bit Settings

// 'C' source line config statements

// CONFIG1
#pragma config FEXTOSC = OFF    // External Oscillator mode selection bits (Oscillator not enabled)
#pragma config RSTOSC = HFINT32 // Power-up default value for COSC bits (HFINTOSC with OSCFRQ= 32 MHz and CDIV = 1:1)
#pragma config CLKOUTEN = OFF   // Clock Out Enable bit (CLKOUT function is disabled; i/o or oscillator function on OSC2)
#pragma config CSWEN = OFF      // Clock Switch Enable bit (The NOSC and NDIV bits cannot be changed by user software)
#pragma config FCMEN = ON       // Fail-Safe Clock Monitor Enable bit (FSCM timer enabled)

// CONFIG2
#pragma config MCLRE = ON       // Master Clear Enable bit (MCLR pin is Master Clear function)
#pragma config PWRTE = ON       // Power-up Timer Enable bit (PWRT enabled)
#pragma config LPBOREN = OFF    // Low-Power BOR enable bit (ULPBOR disabled)
#pragma config BOREN = ON       // Brown-out reset enable bits (Brown-out Reset Enabled, SBOREN bit is ignored)
#pragma config BORV = LO        // Brown-out Reset Voltage Selection (Brown-out Reset Voltage (VBOR) set to 1.9V on LF, and 2.45V on F Devices)
#pragma config ZCD = OFF        // Zero-cross detect disable (Zero-cross detect circuit is disabled at POR.)
#pragma config PPS1WAY = OFF    // Peripheral Pin Select one-way control (The PPSLOCK bit can be set and cleared repeatedly by software)
#pragma config STVREN = ON      // Stack Overflow/Underflow Reset Enable bit (Stack Overflow or Underflow will cause a reset)

// CONFIG3
#pragma config WDTCPS = WDTCPS_31// WDT Period Select bits (Divider ratio 1:65536; software control of WDTPS)
#pragma config WDTE = OFF       // WDT operating mode (WDT Disabled, SWDTEN is ignored)
#pragma config WDTCWS = WDTCWS_7// WDT Window Select bits (window always open (100%); software control; keyed access not required)
#pragma config WDTCCS = SC      // WDT input clock selector (Software Control)

// CONFIG4
#pragma config BBSIZE = BB512   // Boot Block Size Selection bits (512 words boot block size)
#pragma config BBEN = OFF       // Boot Block Enable bit (Boot Block disabled)
#pragma config SAFEN = OFF      // SAF Enable bit (SAF disabled)
#pragma config WRTAPP = OFF     // Application Block Write Protection bit (Application Block not write protected)
#pragma config WRTB = OFF       // Boot Block Write Protection bit (Boot Block not write protected)
#pragma config WRTC = OFF       // Configuration Register Write Protection bit (Configuration Register not write protected)
#pragma config WRTSAF = OFF     // Storage Area Flash Write Protection bit (SAF not write protected)
#pragma config LVP = ON         // Low Voltage Programming Enable bit (Low Voltage programming enabled. MCLR/Vpp pin function is MCLR.)

// CONFIG5
#pragma config CP = OFF         // UserNVM Program memory code protection bit (UserNVM code protection disabled)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include <xc.h>

#include <stdint.h>
#include <stdbool.h>

#include "Bin_Const.h"
#include "ES_Port.h"
#include "ES_Types.h"
#include "ES_Timers.h"


// TickCount is used to track the number of timer ints that have occurred
// since the last check. It should really never be more than 1, but just to
// be sure, we increment it in the interrupt response rather than simply
// setting a flag. Using this variable and checking approach we remove the
// need to post events from the interrupt response routine. This is necessary
// for compilers like HTC for the midrange PICs which do not produce re-entrant
// code so cannot post directly to the queues from within the interrupt resp.
static volatile uint8_t TickCount;

// Global tick count to monitor number of SysTick Interrupts
// make uint16_t to maintain backwards compatibility and not overly burden
// 8 and 16 bit processors
static volatile uint16_t SysTickCounter = 0;

// This variable is used to store the state of the interrupt mask when
// doing EnterCritical/ExitCritical pairs
uint8_t _INTCON_temp;


/****************************************************************************
 * Module Level defines
 ***************************************************************************/
// with SYNC=0, BRGH=0 & BRG16=0
// 2400 Baud = 207
// 9600 Baud = 51
//19200 Baud = 25

#define BAUD_CONST  51

/****************************************************************************
 Function
    _HW_PIC15356Init
 Parameters
    none
 Returns
     None.
 Description
    Initializes the basic hardware on the PIC. Sets up clock to 32MHz
 Notes
     
 Author
     J. Edward Carryer, 04/18/19 16:17
 ****************************************************************************/
void _HW_PIC15356Init(void) {
    _HW_ConsoleInit();
#if 0
    while (1) {
        if (kbhit()) {
            putch(getchar() & BIT5LO);
        }
    }
#endif
}

/****************************************************************************
 Function
     _HW_Timer_Init
 Parameters
     unsigned char Rate set to one of the TMR_RATE_XX values to set the
     Tick rate
 Returns
     None.
 Description
     Initializes the NCO to generate the SysTicks
    rate
 Notes
     modify as required to port to other timer hardware
 Author
     J. Edward Carryer, 04/21/19  08:31
 ****************************************************************************/
void _HW_Timer_Init(const TimerRate_t Rate) {
    // Set up the NCO
    NCO1CLK = B8(0100); // Choose 31.25kHz MFINTOSC as clock

    // program the requested period, done in 3 steps for the 3 registers
    NCO1INCU = 0; // start with the 4 MSB, ours are always 0
    NCO1INCH = Rate >> 8; // now the middle 8 bits
    NCO1INCL = Rate; // finally the lower 8, must be last write

    // start the NCO running
    N1EN = 1;

    //  Enable interrupts, there are a bunch of places needed for this module
    NCO1IE = 1;
    PEIE = 1;
    GIE = 1;

}

/****************************************************************************
 Function
     _HW_SysTickIntHandler
 Parameters
     none
 Returns
     None.
 Description
     interrupt response routine for the tick interrupt that will allow the
     framework timers to run.
 Notes
     As currently (4/21/19) implemented this does not actually post events
     but simply sets a flag to indicate that the interrupt has occurred.
     the framework response is handled below in _HW_Process_Pending_Ints
 Author
    J. Edward Carryer, 04/21/19 08:54
 ****************************************************************************/
void _HW_SysTickIntHandler(void) {
    NCOIF = 0; // clear the source of the interrupt
    ++TickCount; /* flag that it occurred and needs a response */
    ++SysTickCounter; // keep the free running time going
#ifdef LED_DEBUG
    BlinkLED();
#endif
}

/****************************************************************************
 Function
    _HW_GetTickCount()
 Parameters
    none
 Returns
    uint16_t   count of number of system ticks that have occurred.
 Description
    wrapper for access to SysTickCounter, needed to move increment of tick
    counter to this module to keep the timer ticking during blocking code
 Notes

 Author
    Ed Carryer, 10/27/14 13:55
 ****************************************************************************/
uint16_t _HW_GetTickCount(void) {
    return SysTickCounter;
}

/****************************************************************************
 Function
     _HW_Process_Pending_Ints
 Parameters
     none
 Returns
     always true.
 Description
     processes any pending interrupts (actually the hardware interrupt already
     occurred and simply set a flag to tell this routine to execute the non-
     hardware response)
 Notes
     While this routine technically does not need a return value, we always
     return true so that it can be used in the conditional while() loop in
     ES_Run. This way the test for pending interrupts get processed after every
     run function is called and even when there are no queues with events.
     This routine could be expanded to process any other interrupt sources
     that you would like to use to post events to the framework services.
 Author
     J. Edward Carryer, 08/13/13 13:27
 ****************************************************************************/
bool _HW_Process_Pending_Ints(void) {
    while (TickCount > 0) {
        /* call the framework tick response to actually run the timers */
        ES_Timer_Tick_Resp();
        TickCount--;
    }
    return true; // always return true to allow loop test in ES_Run to proceed
}

/****************************************************************************
 Function
     _HW_ConsoleInit
 Parameters
     none
 Returns
     none.
 Description
  Initializes the UART for console I/O
 Notes
  for the PIC16F15356 we are using EUSART1 which defaults to Xmit on RC6 and
  receive on RC7
 Author
     J. Edward Carryer, 04/20/19 10:32
 ****************************************************************************/
void _HW_ConsoleInit(void) {
    // Configure pins for UART 
    ANSELC &= (BIT6LO & BIT7LO); //disable analog on RC6 & 7
    RC6PPS = 0x0f; // maps TX1 to RC6
    // RX1 maps to RC7 by default. If you want to use another pin, then 
    // program that here

    // Initialize the SPBRG(H) register(s) for the appropriate baud rate. 
    // If a high-speed baud rate is desired, set bit BRGH 
    SP1BRGL = BAUD_CONST;
    // Enable the asynchronous serial port by clearing bit SYNC and setting SPEN  
    TX1STAbits.SYNC = 0;
    RC1STAbits.SPEN = 1;
    // If interrupts are desired, then set enable bit TXIE in PIE1.
    // If 9-bit transmission/reception is desired, then set transmit bit TX9

    // Enable the transmission by setting bit TXEN which will also set 
    // bit TXIF in PIR1.
    TX1STAbits.TXEN = 1;
    // Enable the reception by setting the CREN bit.
    RC1STAbits.CREN = 1;

    // If 9-bit transmission is used, the ninth bit should be loaded into TX9D.
    // If using interrupts, ensure that GIE and PEIE 
    // of the INTCON register are set.

}

/****************************************************************************
 Function
  putch
 Parameters
  char the character to print
 Returns
     none.
 Description
  send a single character to EUSART1
 Notes
  This function, named in this way is how we connect printf, puts, etc. to
 the serial port on the PIC
 Author
     J. Edward Carryer, 04/20/19 14:20
 ****************************************************************************/
void putch(char data) {
    while (!TX1IF) // check if last character finished
    {
    } // wait till done
    TX1REG = data; // send data
}

/****************************************************************************
 Function
  getchar
 Parameters
  none
 Returns
  int The character received (use int return to match C99)  
 Description
  send a single character to EUSART1
 Notes
  The prototype for this is in stdio.h
 Author
  J. Edward Carryer, 04/20/19 14:43
 ****************************************************************************/
int getchar(void) {
    while (!RC1IF) // check buffer
    {
    } // wait till ready
    return RC1REG;
}

/****************************************************************************
 Function
 kbhit
 Parameters
  none
 Returns
  bool true if character ready, false otherwise.
 Description
  check the state of the RC1IF flag on EUSART1
 Notes

 Author
     J. Edward Carryer, 04/20/19 14:31
 ****************************************************************************/
bool kbhit(void) {
    /* checks for a character from the terminal channel */
    if (RC1IF) {
        return 1;
    } else {
        return 0;
    }
}
