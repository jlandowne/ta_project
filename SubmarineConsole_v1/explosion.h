#ifndef EXPLOSION_H
#define EXPLOSION_H

#include <QObject>
#include <QGraphicsPixmapItem>
#include <coordinates.h>

class Explosion: public QObject, public QGraphicsPixmapItem {
    Q_OBJECT
public:
    Explosion(QGraphicsItem *parent = 0);
    void set(int x, int y);
    void show();
    void hide();
    Coordinates * coords;
};

#endif // ENEMY_H
