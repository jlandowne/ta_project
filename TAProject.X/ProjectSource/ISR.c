/****************************************************************************
 Module
  ISR.c

 Revision
  1.0.1

 Description
  This is the home for the single ISR for the PIC. You can enter the code here 
  directly if the responses don't need to access module level variables, or 
  call a function in the module to get access to those module level variables
   
 Notes
  make sure that you handle all of the possible sources!
 ***************************************************************************/
 
#include "ES_Port.h"
#include "ReceiveFSM.h"
void __interrupt() myIsr(void)
 {
   if (NCO1IF == 1){
     _HW_SysTickIntHandler();
   }
   if (PIR6bits.CCP1IF == 1) {
       RXISR();
   }
 }
 