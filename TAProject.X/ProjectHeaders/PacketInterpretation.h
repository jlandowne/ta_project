/****************************************************************************

  Header file for Packet Interpretation service
  based on the Gen2 Events and Services Framework

 ****************************************************************************/

#ifndef PacketInterpretation_H
#define PacketInterpretation_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Port.h"
#include "ES_Events.h"

// Public Function Prototypes
bool InitPacketInterpretation(uint8_t Priority);
REENTRANT bool PostPacketInterpretation(ES_Event_t ThisEvent);
ES_Event_t RunPacketInterpretation(ES_Event_t ThisEvent);

#endif
