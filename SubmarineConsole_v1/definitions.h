#ifndef DEFINITIONS_H
#define DEFINITIONS_H

#include <map>
#include <iostream>
#include <QString>

//Please input your default serial port name!
#define DEFAULT_SERIAL_PORT "/dev/cu.usbserial-0001"

#define BIT0HI 1
#define BIT0LO ~1
#define BIT1HI 2
#define BIT1LO ~2
#define BIT2HI 4
#define BIT2LO ~4
#define BIT3HI 8
#define BIT3LO ~8
#define BIT4HI 16
#define BIT4LO ~16
#define BIT5HI 32
#define BIT5LO ~32
#define BIT6HI 64
#define BIT6LO ~64
#define BIT7HI 128
#define BIT7LO ~128

//Screen defines
#define ScreenScale 1
#define ScreenWidth 1300
#define ScreenHeight 750
#define CENTER_X 650
#define CENTER_Y 375

#define STD_WIDTH 100
#define STD_HEIGHT 50
#define TEXT_OFFSET 10
#define ITEM_OFFSET 150
#define HORIZ_OFFSET 75

//Origin of hex grid
#define ORIGIN_X 400
#define ORIGIN_Y 340

#define DEAD 0
#define ALIVE 1

//std::map<QString,int> headingmap = {{"UP", 90}, {"RIGHT UP", 150},
                                    //{"RIGHT DOWN", 210}, {"DOWN",270},
                                    //{"LEFT DOWN",330}, {"LEFT UP",30}};


#endif // DEFINITIONS_H
