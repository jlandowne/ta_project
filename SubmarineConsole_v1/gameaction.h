#ifndef GAMEACTION_H
#define GAMEACTION_H

#include "button.h"
#include <QObject>

class GameAction: public QObject {
    Q_OBJECT
public:
    GameAction();
public slots:
    void d_pinged();
};
#endif // GAMEACTION_H
