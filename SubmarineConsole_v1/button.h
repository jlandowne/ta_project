#ifndef BUTTON_H
#define BUTTON_H

#include <QGraphicsRectItem>
#include <QGraphicsSceneMouseEvent>
#include <QObject>
#include <QColor>

#define OMNI_ACTION 2
#define DIRECTED_ACTION 1
#define IDLE_ACTION 0
#define MOVE_ACTION 2
#define TURN_ACTION 1
#define FIRE_ACTION 2

class Button: public QObject, public QGraphicsRectItem {
    Q_OBJECT
public:
    Button(QString name, QGraphicsItem * parent = NULL);

    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void hoverEnterEvent(QGraphicsSceneHoverEvent *event);
    void hoverLeaveEvent(QGraphicsSceneHoverEvent *event);
    void setColor(QColor c);

    QGraphicsTextItem * text;
    QColor color;

signals:
    void clicked();
};
#endif // BUTTON_H
