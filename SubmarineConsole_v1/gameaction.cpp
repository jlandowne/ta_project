#include "gameaction.h"
#include "game.h"
#include "button.h"
#include "coordinates.h"

#include <QGraphicsTextItem>
#include <QString>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QObject>
#include <QGraphicsTextItem>
#include <QDebug>

extern Game * game;

GameAction::GameAction()
{
    //Add control box titles
    QGraphicsTextItem* sonarText = new QGraphicsTextItem(QString("SONAR"));
    QFont titleFont("comic sans", 24);
    sonarText->setFont(titleFont);
    int txPos = ScreenWidth*ScreenScale/2 - sonarText->boundingRect().width()/2 - ScreenWidth*ScreenScale/6;
    int tyPos = ScreenHeight*ScreenScale - 25*ScreenScale;
    sonarText->setPos(txPos, tyPos);
    game->scene->addItem(sonarText);

    //Add buttons
    Button * DPingButton = new Button(QString("PingDirected"));
    DPingButton->setPos(txPos, tyPos + 3*ScreenScale);
    QObject::connect( DPingButton, SIGNAL( clicked() ), game, SLOT( DPingButton->d_pinged() )) ;
    game->scene->addItem(DPingButton);
}

void GameAction::d_pinged()
{
    qDebug() << "d_ping received";
}
