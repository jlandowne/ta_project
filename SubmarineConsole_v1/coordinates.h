#ifndef COORDINATES_H
#define COORDINATES_H

class Coordinates {
public:
    int X_Pixels(int x, int y);
    int Y_Pixels(int x, int y);
};
#endif // COORDINATES_H
