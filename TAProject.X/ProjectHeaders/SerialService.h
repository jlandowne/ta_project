/****************************************************************************

  Header file for Test Harness Service0
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef SerialService_H
#define SerialService_H

#include <stdint.h>
#include <stdbool.h>

#include "ES_Events.h"
#include "ES_Port.h"                // needed for definition of REENTRANT
// Public Function Prototypes

bool InitSerialService(uint8_t Priority);
REENTRANT bool PostSerialService(ES_Event_t ThisEvent);
ES_Event_t RunSerialService(ES_Event_t ThisEvent);

#endif /* ServTemplate_H */

