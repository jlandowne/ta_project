#ifndef STARTMENU_H
#define STARTMENU_H

#include <QLineEdit>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QObject>
#include <QComboBox>
#include "button.h"
#include "definitions.h"

class StartMenu: public QGraphicsView {
    Q_OBJECT
public:
    StartMenu(QWidget * parent=0);

    QGraphicsScene * scene;
    QLineEdit * CommPort;
    QLineEdit * StartX;
    QLineEdit * StartY;
    QComboBox * StartHeading;
    QLineEdit * TeamNum;
    QLineEdit * NumTeams;
    Button * StartButton;

public slots:
    void start_game();
};
#endif // STARTMENU_H
