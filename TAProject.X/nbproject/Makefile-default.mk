#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/TAProject.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/TAProject.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=-mafrlcsj
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=FrameworkSource/ES_Port.c FrameworkSource/ES_Timers.c FrameworkSource/ES_LookupTables.c FrameworkSource/ES_CheckEvents.c FrameworkSource/ES_DeferRecall.c FrameworkSource/ES_Framework.c FrameworkSource/ES_Queue.c FrameworkSource/ES_PostList.c ProjectSource/main.c ProjectSource/EventCheckers.c ProjectSource/TestHarnessService0.c ProjectSource/ISR.c ProjectSource/dbprintf.c ProjectSource/ReceiveFSM.c ProjectSource/PacketInterpretation.c ProjectSource/SerialService.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/FrameworkSource/ES_Port.p1 ${OBJECTDIR}/FrameworkSource/ES_Timers.p1 ${OBJECTDIR}/FrameworkSource/ES_LookupTables.p1 ${OBJECTDIR}/FrameworkSource/ES_CheckEvents.p1 ${OBJECTDIR}/FrameworkSource/ES_DeferRecall.p1 ${OBJECTDIR}/FrameworkSource/ES_Framework.p1 ${OBJECTDIR}/FrameworkSource/ES_Queue.p1 ${OBJECTDIR}/FrameworkSource/ES_PostList.p1 ${OBJECTDIR}/ProjectSource/main.p1 ${OBJECTDIR}/ProjectSource/EventCheckers.p1 ${OBJECTDIR}/ProjectSource/TestHarnessService0.p1 ${OBJECTDIR}/ProjectSource/ISR.p1 ${OBJECTDIR}/ProjectSource/dbprintf.p1 ${OBJECTDIR}/ProjectSource/ReceiveFSM.p1 ${OBJECTDIR}/ProjectSource/PacketInterpretation.p1 ${OBJECTDIR}/ProjectSource/SerialService.p1
POSSIBLE_DEPFILES=${OBJECTDIR}/FrameworkSource/ES_Port.p1.d ${OBJECTDIR}/FrameworkSource/ES_Timers.p1.d ${OBJECTDIR}/FrameworkSource/ES_LookupTables.p1.d ${OBJECTDIR}/FrameworkSource/ES_CheckEvents.p1.d ${OBJECTDIR}/FrameworkSource/ES_DeferRecall.p1.d ${OBJECTDIR}/FrameworkSource/ES_Framework.p1.d ${OBJECTDIR}/FrameworkSource/ES_Queue.p1.d ${OBJECTDIR}/FrameworkSource/ES_PostList.p1.d ${OBJECTDIR}/ProjectSource/main.p1.d ${OBJECTDIR}/ProjectSource/EventCheckers.p1.d ${OBJECTDIR}/ProjectSource/TestHarnessService0.p1.d ${OBJECTDIR}/ProjectSource/ISR.p1.d ${OBJECTDIR}/ProjectSource/dbprintf.p1.d ${OBJECTDIR}/ProjectSource/ReceiveFSM.p1.d ${OBJECTDIR}/ProjectSource/PacketInterpretation.p1.d ${OBJECTDIR}/ProjectSource/SerialService.p1.d

# Object Files
OBJECTFILES=${OBJECTDIR}/FrameworkSource/ES_Port.p1 ${OBJECTDIR}/FrameworkSource/ES_Timers.p1 ${OBJECTDIR}/FrameworkSource/ES_LookupTables.p1 ${OBJECTDIR}/FrameworkSource/ES_CheckEvents.p1 ${OBJECTDIR}/FrameworkSource/ES_DeferRecall.p1 ${OBJECTDIR}/FrameworkSource/ES_Framework.p1 ${OBJECTDIR}/FrameworkSource/ES_Queue.p1 ${OBJECTDIR}/FrameworkSource/ES_PostList.p1 ${OBJECTDIR}/ProjectSource/main.p1 ${OBJECTDIR}/ProjectSource/EventCheckers.p1 ${OBJECTDIR}/ProjectSource/TestHarnessService0.p1 ${OBJECTDIR}/ProjectSource/ISR.p1 ${OBJECTDIR}/ProjectSource/dbprintf.p1 ${OBJECTDIR}/ProjectSource/ReceiveFSM.p1 ${OBJECTDIR}/ProjectSource/PacketInterpretation.p1 ${OBJECTDIR}/ProjectSource/SerialService.p1

# Source Files
SOURCEFILES=FrameworkSource/ES_Port.c FrameworkSource/ES_Timers.c FrameworkSource/ES_LookupTables.c FrameworkSource/ES_CheckEvents.c FrameworkSource/ES_DeferRecall.c FrameworkSource/ES_Framework.c FrameworkSource/ES_Queue.c FrameworkSource/ES_PostList.c ProjectSource/main.c ProjectSource/EventCheckers.c ProjectSource/TestHarnessService0.c ProjectSource/ISR.c ProjectSource/dbprintf.c ProjectSource/ReceiveFSM.c ProjectSource/PacketInterpretation.c ProjectSource/SerialService.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/TAProject.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=16F15356
# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/FrameworkSource/ES_Port.p1: FrameworkSource/ES_Port.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/FrameworkSource" 
	@${RM} ${OBJECTDIR}/FrameworkSource/ES_Port.p1.d 
	@${RM} ${OBJECTDIR}/FrameworkSource/ES_Port.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -O0 -maddrqual=ignore -I"FrameworkHeaders" -I"ProjectHeaders" -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,+file  -ginhx032 -Wl,--data-init -mkeep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=hybrid:auto:auto     -o ${OBJECTDIR}/FrameworkSource/ES_Port.p1 FrameworkSource/ES_Port.c 
	@-${MV} ${OBJECTDIR}/FrameworkSource/ES_Port.d ${OBJECTDIR}/FrameworkSource/ES_Port.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/FrameworkSource/ES_Port.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/FrameworkSource/ES_Timers.p1: FrameworkSource/ES_Timers.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/FrameworkSource" 
	@${RM} ${OBJECTDIR}/FrameworkSource/ES_Timers.p1.d 
	@${RM} ${OBJECTDIR}/FrameworkSource/ES_Timers.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -O0 -maddrqual=ignore -I"FrameworkHeaders" -I"ProjectHeaders" -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,+file  -ginhx032 -Wl,--data-init -mkeep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=hybrid:auto:auto     -o ${OBJECTDIR}/FrameworkSource/ES_Timers.p1 FrameworkSource/ES_Timers.c 
	@-${MV} ${OBJECTDIR}/FrameworkSource/ES_Timers.d ${OBJECTDIR}/FrameworkSource/ES_Timers.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/FrameworkSource/ES_Timers.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/FrameworkSource/ES_LookupTables.p1: FrameworkSource/ES_LookupTables.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/FrameworkSource" 
	@${RM} ${OBJECTDIR}/FrameworkSource/ES_LookupTables.p1.d 
	@${RM} ${OBJECTDIR}/FrameworkSource/ES_LookupTables.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -O0 -maddrqual=ignore -I"FrameworkHeaders" -I"ProjectHeaders" -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,+file  -ginhx032 -Wl,--data-init -mkeep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=hybrid:auto:auto     -o ${OBJECTDIR}/FrameworkSource/ES_LookupTables.p1 FrameworkSource/ES_LookupTables.c 
	@-${MV} ${OBJECTDIR}/FrameworkSource/ES_LookupTables.d ${OBJECTDIR}/FrameworkSource/ES_LookupTables.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/FrameworkSource/ES_LookupTables.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/FrameworkSource/ES_CheckEvents.p1: FrameworkSource/ES_CheckEvents.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/FrameworkSource" 
	@${RM} ${OBJECTDIR}/FrameworkSource/ES_CheckEvents.p1.d 
	@${RM} ${OBJECTDIR}/FrameworkSource/ES_CheckEvents.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -O0 -maddrqual=ignore -I"FrameworkHeaders" -I"ProjectHeaders" -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,+file  -ginhx032 -Wl,--data-init -mkeep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=hybrid:auto:auto     -o ${OBJECTDIR}/FrameworkSource/ES_CheckEvents.p1 FrameworkSource/ES_CheckEvents.c 
	@-${MV} ${OBJECTDIR}/FrameworkSource/ES_CheckEvents.d ${OBJECTDIR}/FrameworkSource/ES_CheckEvents.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/FrameworkSource/ES_CheckEvents.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/FrameworkSource/ES_DeferRecall.p1: FrameworkSource/ES_DeferRecall.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/FrameworkSource" 
	@${RM} ${OBJECTDIR}/FrameworkSource/ES_DeferRecall.p1.d 
	@${RM} ${OBJECTDIR}/FrameworkSource/ES_DeferRecall.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -O0 -maddrqual=ignore -I"FrameworkHeaders" -I"ProjectHeaders" -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,+file  -ginhx032 -Wl,--data-init -mkeep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=hybrid:auto:auto     -o ${OBJECTDIR}/FrameworkSource/ES_DeferRecall.p1 FrameworkSource/ES_DeferRecall.c 
	@-${MV} ${OBJECTDIR}/FrameworkSource/ES_DeferRecall.d ${OBJECTDIR}/FrameworkSource/ES_DeferRecall.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/FrameworkSource/ES_DeferRecall.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/FrameworkSource/ES_Framework.p1: FrameworkSource/ES_Framework.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/FrameworkSource" 
	@${RM} ${OBJECTDIR}/FrameworkSource/ES_Framework.p1.d 
	@${RM} ${OBJECTDIR}/FrameworkSource/ES_Framework.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -O0 -maddrqual=ignore -I"FrameworkHeaders" -I"ProjectHeaders" -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,+file  -ginhx032 -Wl,--data-init -mkeep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=hybrid:auto:auto     -o ${OBJECTDIR}/FrameworkSource/ES_Framework.p1 FrameworkSource/ES_Framework.c 
	@-${MV} ${OBJECTDIR}/FrameworkSource/ES_Framework.d ${OBJECTDIR}/FrameworkSource/ES_Framework.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/FrameworkSource/ES_Framework.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/FrameworkSource/ES_Queue.p1: FrameworkSource/ES_Queue.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/FrameworkSource" 
	@${RM} ${OBJECTDIR}/FrameworkSource/ES_Queue.p1.d 
	@${RM} ${OBJECTDIR}/FrameworkSource/ES_Queue.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -O0 -maddrqual=ignore -I"FrameworkHeaders" -I"ProjectHeaders" -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,+file  -ginhx032 -Wl,--data-init -mkeep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=hybrid:auto:auto     -o ${OBJECTDIR}/FrameworkSource/ES_Queue.p1 FrameworkSource/ES_Queue.c 
	@-${MV} ${OBJECTDIR}/FrameworkSource/ES_Queue.d ${OBJECTDIR}/FrameworkSource/ES_Queue.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/FrameworkSource/ES_Queue.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/FrameworkSource/ES_PostList.p1: FrameworkSource/ES_PostList.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/FrameworkSource" 
	@${RM} ${OBJECTDIR}/FrameworkSource/ES_PostList.p1.d 
	@${RM} ${OBJECTDIR}/FrameworkSource/ES_PostList.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -O0 -maddrqual=ignore -I"FrameworkHeaders" -I"ProjectHeaders" -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,+file  -ginhx032 -Wl,--data-init -mkeep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=hybrid:auto:auto     -o ${OBJECTDIR}/FrameworkSource/ES_PostList.p1 FrameworkSource/ES_PostList.c 
	@-${MV} ${OBJECTDIR}/FrameworkSource/ES_PostList.d ${OBJECTDIR}/FrameworkSource/ES_PostList.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/FrameworkSource/ES_PostList.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/ProjectSource/main.p1: ProjectSource/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/ProjectSource" 
	@${RM} ${OBJECTDIR}/ProjectSource/main.p1.d 
	@${RM} ${OBJECTDIR}/ProjectSource/main.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -O0 -maddrqual=ignore -I"FrameworkHeaders" -I"ProjectHeaders" -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,+file  -ginhx032 -Wl,--data-init -mkeep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=hybrid:auto:auto     -o ${OBJECTDIR}/ProjectSource/main.p1 ProjectSource/main.c 
	@-${MV} ${OBJECTDIR}/ProjectSource/main.d ${OBJECTDIR}/ProjectSource/main.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/ProjectSource/main.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/ProjectSource/EventCheckers.p1: ProjectSource/EventCheckers.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/ProjectSource" 
	@${RM} ${OBJECTDIR}/ProjectSource/EventCheckers.p1.d 
	@${RM} ${OBJECTDIR}/ProjectSource/EventCheckers.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -O0 -maddrqual=ignore -I"FrameworkHeaders" -I"ProjectHeaders" -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,+file  -ginhx032 -Wl,--data-init -mkeep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=hybrid:auto:auto     -o ${OBJECTDIR}/ProjectSource/EventCheckers.p1 ProjectSource/EventCheckers.c 
	@-${MV} ${OBJECTDIR}/ProjectSource/EventCheckers.d ${OBJECTDIR}/ProjectSource/EventCheckers.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/ProjectSource/EventCheckers.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/ProjectSource/TestHarnessService0.p1: ProjectSource/TestHarnessService0.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/ProjectSource" 
	@${RM} ${OBJECTDIR}/ProjectSource/TestHarnessService0.p1.d 
	@${RM} ${OBJECTDIR}/ProjectSource/TestHarnessService0.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -O0 -maddrqual=ignore -I"FrameworkHeaders" -I"ProjectHeaders" -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,+file  -ginhx032 -Wl,--data-init -mkeep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=hybrid:auto:auto     -o ${OBJECTDIR}/ProjectSource/TestHarnessService0.p1 ProjectSource/TestHarnessService0.c 
	@-${MV} ${OBJECTDIR}/ProjectSource/TestHarnessService0.d ${OBJECTDIR}/ProjectSource/TestHarnessService0.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/ProjectSource/TestHarnessService0.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/ProjectSource/ISR.p1: ProjectSource/ISR.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/ProjectSource" 
	@${RM} ${OBJECTDIR}/ProjectSource/ISR.p1.d 
	@${RM} ${OBJECTDIR}/ProjectSource/ISR.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -O0 -maddrqual=ignore -I"FrameworkHeaders" -I"ProjectHeaders" -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,+file  -ginhx032 -Wl,--data-init -mkeep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=hybrid:auto:auto     -o ${OBJECTDIR}/ProjectSource/ISR.p1 ProjectSource/ISR.c 
	@-${MV} ${OBJECTDIR}/ProjectSource/ISR.d ${OBJECTDIR}/ProjectSource/ISR.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/ProjectSource/ISR.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/ProjectSource/dbprintf.p1: ProjectSource/dbprintf.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/ProjectSource" 
	@${RM} ${OBJECTDIR}/ProjectSource/dbprintf.p1.d 
	@${RM} ${OBJECTDIR}/ProjectSource/dbprintf.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -O0 -maddrqual=ignore -I"FrameworkHeaders" -I"ProjectHeaders" -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,+file  -ginhx032 -Wl,--data-init -mkeep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=hybrid:auto:auto     -o ${OBJECTDIR}/ProjectSource/dbprintf.p1 ProjectSource/dbprintf.c 
	@-${MV} ${OBJECTDIR}/ProjectSource/dbprintf.d ${OBJECTDIR}/ProjectSource/dbprintf.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/ProjectSource/dbprintf.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/ProjectSource/ReceiveFSM.p1: ProjectSource/ReceiveFSM.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/ProjectSource" 
	@${RM} ${OBJECTDIR}/ProjectSource/ReceiveFSM.p1.d 
	@${RM} ${OBJECTDIR}/ProjectSource/ReceiveFSM.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -O0 -maddrqual=ignore -I"FrameworkHeaders" -I"ProjectHeaders" -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,+file  -ginhx032 -Wl,--data-init -mkeep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=hybrid:auto:auto     -o ${OBJECTDIR}/ProjectSource/ReceiveFSM.p1 ProjectSource/ReceiveFSM.c 
	@-${MV} ${OBJECTDIR}/ProjectSource/ReceiveFSM.d ${OBJECTDIR}/ProjectSource/ReceiveFSM.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/ProjectSource/ReceiveFSM.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/ProjectSource/PacketInterpretation.p1: ProjectSource/PacketInterpretation.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/ProjectSource" 
	@${RM} ${OBJECTDIR}/ProjectSource/PacketInterpretation.p1.d 
	@${RM} ${OBJECTDIR}/ProjectSource/PacketInterpretation.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -O0 -maddrqual=ignore -I"FrameworkHeaders" -I"ProjectHeaders" -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,+file  -ginhx032 -Wl,--data-init -mkeep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=hybrid:auto:auto     -o ${OBJECTDIR}/ProjectSource/PacketInterpretation.p1 ProjectSource/PacketInterpretation.c 
	@-${MV} ${OBJECTDIR}/ProjectSource/PacketInterpretation.d ${OBJECTDIR}/ProjectSource/PacketInterpretation.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/ProjectSource/PacketInterpretation.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/ProjectSource/SerialService.p1: ProjectSource/SerialService.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/ProjectSource" 
	@${RM} ${OBJECTDIR}/ProjectSource/SerialService.p1.d 
	@${RM} ${OBJECTDIR}/ProjectSource/SerialService.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -fno-short-double -fno-short-float -O0 -maddrqual=ignore -I"FrameworkHeaders" -I"ProjectHeaders" -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,+file  -ginhx032 -Wl,--data-init -mkeep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=hybrid:auto:auto     -o ${OBJECTDIR}/ProjectSource/SerialService.p1 ProjectSource/SerialService.c 
	@-${MV} ${OBJECTDIR}/ProjectSource/SerialService.d ${OBJECTDIR}/ProjectSource/SerialService.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/ProjectSource/SerialService.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
else
${OBJECTDIR}/FrameworkSource/ES_Port.p1: FrameworkSource/ES_Port.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/FrameworkSource" 
	@${RM} ${OBJECTDIR}/FrameworkSource/ES_Port.p1.d 
	@${RM} ${OBJECTDIR}/FrameworkSource/ES_Port.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -O0 -maddrqual=ignore -I"FrameworkHeaders" -I"ProjectHeaders" -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,+file  -ginhx032 -Wl,--data-init -mkeep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=hybrid:auto:auto     -o ${OBJECTDIR}/FrameworkSource/ES_Port.p1 FrameworkSource/ES_Port.c 
	@-${MV} ${OBJECTDIR}/FrameworkSource/ES_Port.d ${OBJECTDIR}/FrameworkSource/ES_Port.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/FrameworkSource/ES_Port.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/FrameworkSource/ES_Timers.p1: FrameworkSource/ES_Timers.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/FrameworkSource" 
	@${RM} ${OBJECTDIR}/FrameworkSource/ES_Timers.p1.d 
	@${RM} ${OBJECTDIR}/FrameworkSource/ES_Timers.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -O0 -maddrqual=ignore -I"FrameworkHeaders" -I"ProjectHeaders" -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,+file  -ginhx032 -Wl,--data-init -mkeep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=hybrid:auto:auto     -o ${OBJECTDIR}/FrameworkSource/ES_Timers.p1 FrameworkSource/ES_Timers.c 
	@-${MV} ${OBJECTDIR}/FrameworkSource/ES_Timers.d ${OBJECTDIR}/FrameworkSource/ES_Timers.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/FrameworkSource/ES_Timers.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/FrameworkSource/ES_LookupTables.p1: FrameworkSource/ES_LookupTables.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/FrameworkSource" 
	@${RM} ${OBJECTDIR}/FrameworkSource/ES_LookupTables.p1.d 
	@${RM} ${OBJECTDIR}/FrameworkSource/ES_LookupTables.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -O0 -maddrqual=ignore -I"FrameworkHeaders" -I"ProjectHeaders" -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,+file  -ginhx032 -Wl,--data-init -mkeep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=hybrid:auto:auto     -o ${OBJECTDIR}/FrameworkSource/ES_LookupTables.p1 FrameworkSource/ES_LookupTables.c 
	@-${MV} ${OBJECTDIR}/FrameworkSource/ES_LookupTables.d ${OBJECTDIR}/FrameworkSource/ES_LookupTables.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/FrameworkSource/ES_LookupTables.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/FrameworkSource/ES_CheckEvents.p1: FrameworkSource/ES_CheckEvents.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/FrameworkSource" 
	@${RM} ${OBJECTDIR}/FrameworkSource/ES_CheckEvents.p1.d 
	@${RM} ${OBJECTDIR}/FrameworkSource/ES_CheckEvents.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -O0 -maddrqual=ignore -I"FrameworkHeaders" -I"ProjectHeaders" -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,+file  -ginhx032 -Wl,--data-init -mkeep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=hybrid:auto:auto     -o ${OBJECTDIR}/FrameworkSource/ES_CheckEvents.p1 FrameworkSource/ES_CheckEvents.c 
	@-${MV} ${OBJECTDIR}/FrameworkSource/ES_CheckEvents.d ${OBJECTDIR}/FrameworkSource/ES_CheckEvents.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/FrameworkSource/ES_CheckEvents.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/FrameworkSource/ES_DeferRecall.p1: FrameworkSource/ES_DeferRecall.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/FrameworkSource" 
	@${RM} ${OBJECTDIR}/FrameworkSource/ES_DeferRecall.p1.d 
	@${RM} ${OBJECTDIR}/FrameworkSource/ES_DeferRecall.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -O0 -maddrqual=ignore -I"FrameworkHeaders" -I"ProjectHeaders" -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,+file  -ginhx032 -Wl,--data-init -mkeep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=hybrid:auto:auto     -o ${OBJECTDIR}/FrameworkSource/ES_DeferRecall.p1 FrameworkSource/ES_DeferRecall.c 
	@-${MV} ${OBJECTDIR}/FrameworkSource/ES_DeferRecall.d ${OBJECTDIR}/FrameworkSource/ES_DeferRecall.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/FrameworkSource/ES_DeferRecall.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/FrameworkSource/ES_Framework.p1: FrameworkSource/ES_Framework.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/FrameworkSource" 
	@${RM} ${OBJECTDIR}/FrameworkSource/ES_Framework.p1.d 
	@${RM} ${OBJECTDIR}/FrameworkSource/ES_Framework.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -O0 -maddrqual=ignore -I"FrameworkHeaders" -I"ProjectHeaders" -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,+file  -ginhx032 -Wl,--data-init -mkeep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=hybrid:auto:auto     -o ${OBJECTDIR}/FrameworkSource/ES_Framework.p1 FrameworkSource/ES_Framework.c 
	@-${MV} ${OBJECTDIR}/FrameworkSource/ES_Framework.d ${OBJECTDIR}/FrameworkSource/ES_Framework.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/FrameworkSource/ES_Framework.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/FrameworkSource/ES_Queue.p1: FrameworkSource/ES_Queue.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/FrameworkSource" 
	@${RM} ${OBJECTDIR}/FrameworkSource/ES_Queue.p1.d 
	@${RM} ${OBJECTDIR}/FrameworkSource/ES_Queue.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -O0 -maddrqual=ignore -I"FrameworkHeaders" -I"ProjectHeaders" -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,+file  -ginhx032 -Wl,--data-init -mkeep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=hybrid:auto:auto     -o ${OBJECTDIR}/FrameworkSource/ES_Queue.p1 FrameworkSource/ES_Queue.c 
	@-${MV} ${OBJECTDIR}/FrameworkSource/ES_Queue.d ${OBJECTDIR}/FrameworkSource/ES_Queue.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/FrameworkSource/ES_Queue.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/FrameworkSource/ES_PostList.p1: FrameworkSource/ES_PostList.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/FrameworkSource" 
	@${RM} ${OBJECTDIR}/FrameworkSource/ES_PostList.p1.d 
	@${RM} ${OBJECTDIR}/FrameworkSource/ES_PostList.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -O0 -maddrqual=ignore -I"FrameworkHeaders" -I"ProjectHeaders" -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,+file  -ginhx032 -Wl,--data-init -mkeep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=hybrid:auto:auto     -o ${OBJECTDIR}/FrameworkSource/ES_PostList.p1 FrameworkSource/ES_PostList.c 
	@-${MV} ${OBJECTDIR}/FrameworkSource/ES_PostList.d ${OBJECTDIR}/FrameworkSource/ES_PostList.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/FrameworkSource/ES_PostList.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/ProjectSource/main.p1: ProjectSource/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/ProjectSource" 
	@${RM} ${OBJECTDIR}/ProjectSource/main.p1.d 
	@${RM} ${OBJECTDIR}/ProjectSource/main.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -O0 -maddrqual=ignore -I"FrameworkHeaders" -I"ProjectHeaders" -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,+file  -ginhx032 -Wl,--data-init -mkeep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=hybrid:auto:auto     -o ${OBJECTDIR}/ProjectSource/main.p1 ProjectSource/main.c 
	@-${MV} ${OBJECTDIR}/ProjectSource/main.d ${OBJECTDIR}/ProjectSource/main.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/ProjectSource/main.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/ProjectSource/EventCheckers.p1: ProjectSource/EventCheckers.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/ProjectSource" 
	@${RM} ${OBJECTDIR}/ProjectSource/EventCheckers.p1.d 
	@${RM} ${OBJECTDIR}/ProjectSource/EventCheckers.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -O0 -maddrqual=ignore -I"FrameworkHeaders" -I"ProjectHeaders" -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,+file  -ginhx032 -Wl,--data-init -mkeep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=hybrid:auto:auto     -o ${OBJECTDIR}/ProjectSource/EventCheckers.p1 ProjectSource/EventCheckers.c 
	@-${MV} ${OBJECTDIR}/ProjectSource/EventCheckers.d ${OBJECTDIR}/ProjectSource/EventCheckers.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/ProjectSource/EventCheckers.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/ProjectSource/TestHarnessService0.p1: ProjectSource/TestHarnessService0.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/ProjectSource" 
	@${RM} ${OBJECTDIR}/ProjectSource/TestHarnessService0.p1.d 
	@${RM} ${OBJECTDIR}/ProjectSource/TestHarnessService0.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -O0 -maddrqual=ignore -I"FrameworkHeaders" -I"ProjectHeaders" -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,+file  -ginhx032 -Wl,--data-init -mkeep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=hybrid:auto:auto     -o ${OBJECTDIR}/ProjectSource/TestHarnessService0.p1 ProjectSource/TestHarnessService0.c 
	@-${MV} ${OBJECTDIR}/ProjectSource/TestHarnessService0.d ${OBJECTDIR}/ProjectSource/TestHarnessService0.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/ProjectSource/TestHarnessService0.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/ProjectSource/ISR.p1: ProjectSource/ISR.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/ProjectSource" 
	@${RM} ${OBJECTDIR}/ProjectSource/ISR.p1.d 
	@${RM} ${OBJECTDIR}/ProjectSource/ISR.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -O0 -maddrqual=ignore -I"FrameworkHeaders" -I"ProjectHeaders" -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,+file  -ginhx032 -Wl,--data-init -mkeep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=hybrid:auto:auto     -o ${OBJECTDIR}/ProjectSource/ISR.p1 ProjectSource/ISR.c 
	@-${MV} ${OBJECTDIR}/ProjectSource/ISR.d ${OBJECTDIR}/ProjectSource/ISR.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/ProjectSource/ISR.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/ProjectSource/dbprintf.p1: ProjectSource/dbprintf.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/ProjectSource" 
	@${RM} ${OBJECTDIR}/ProjectSource/dbprintf.p1.d 
	@${RM} ${OBJECTDIR}/ProjectSource/dbprintf.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -O0 -maddrqual=ignore -I"FrameworkHeaders" -I"ProjectHeaders" -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,+file  -ginhx032 -Wl,--data-init -mkeep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=hybrid:auto:auto     -o ${OBJECTDIR}/ProjectSource/dbprintf.p1 ProjectSource/dbprintf.c 
	@-${MV} ${OBJECTDIR}/ProjectSource/dbprintf.d ${OBJECTDIR}/ProjectSource/dbprintf.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/ProjectSource/dbprintf.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/ProjectSource/ReceiveFSM.p1: ProjectSource/ReceiveFSM.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/ProjectSource" 
	@${RM} ${OBJECTDIR}/ProjectSource/ReceiveFSM.p1.d 
	@${RM} ${OBJECTDIR}/ProjectSource/ReceiveFSM.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -O0 -maddrqual=ignore -I"FrameworkHeaders" -I"ProjectHeaders" -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,+file  -ginhx032 -Wl,--data-init -mkeep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=hybrid:auto:auto     -o ${OBJECTDIR}/ProjectSource/ReceiveFSM.p1 ProjectSource/ReceiveFSM.c 
	@-${MV} ${OBJECTDIR}/ProjectSource/ReceiveFSM.d ${OBJECTDIR}/ProjectSource/ReceiveFSM.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/ProjectSource/ReceiveFSM.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/ProjectSource/PacketInterpretation.p1: ProjectSource/PacketInterpretation.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/ProjectSource" 
	@${RM} ${OBJECTDIR}/ProjectSource/PacketInterpretation.p1.d 
	@${RM} ${OBJECTDIR}/ProjectSource/PacketInterpretation.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -O0 -maddrqual=ignore -I"FrameworkHeaders" -I"ProjectHeaders" -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,+file  -ginhx032 -Wl,--data-init -mkeep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=hybrid:auto:auto     -o ${OBJECTDIR}/ProjectSource/PacketInterpretation.p1 ProjectSource/PacketInterpretation.c 
	@-${MV} ${OBJECTDIR}/ProjectSource/PacketInterpretation.d ${OBJECTDIR}/ProjectSource/PacketInterpretation.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/ProjectSource/PacketInterpretation.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/ProjectSource/SerialService.p1: ProjectSource/SerialService.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/ProjectSource" 
	@${RM} ${OBJECTDIR}/ProjectSource/SerialService.p1.d 
	@${RM} ${OBJECTDIR}/ProjectSource/SerialService.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -fno-short-double -fno-short-float -O0 -maddrqual=ignore -I"FrameworkHeaders" -I"ProjectHeaders" -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,+file  -ginhx032 -Wl,--data-init -mkeep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=hybrid:auto:auto     -o ${OBJECTDIR}/ProjectSource/SerialService.p1 ProjectSource/SerialService.c 
	@-${MV} ${OBJECTDIR}/ProjectSource/SerialService.d ${OBJECTDIR}/ProjectSource/SerialService.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/ProjectSource/SerialService.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/TAProject.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -Wl,-Map=dist/${CND_CONF}/${IMAGE_TYPE}/TAProject.X.${IMAGE_TYPE}.map  -D__DEBUG=1  -DXPRJ_default=$(CND_CONF)  -Wl,--defsym=__MPLAB_BUILD=1  -fno-short-double -fno-short-float -O0 -maddrqual=ignore -I"FrameworkHeaders" -I"ProjectHeaders" -Wa,-a -msummary=-psect,-class,+mem,-hex,+file  -ginhx032 -Wl,--data-init -mkeep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall -std=c99 -gdwarf-3 -mstack=hybrid:auto:auto        $(COMPARISON_BUILD) -Wl,--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml -o dist/${CND_CONF}/${IMAGE_TYPE}/TAProject.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}     
	@${RM} dist/${CND_CONF}/${IMAGE_TYPE}/TAProject.X.${IMAGE_TYPE}.hex 
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/TAProject.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -Wl,-Map=dist/${CND_CONF}/${IMAGE_TYPE}/TAProject.X.${IMAGE_TYPE}.map  -DXPRJ_default=$(CND_CONF)  -Wl,--defsym=__MPLAB_BUILD=1  -fno-short-double -fno-short-float -O0 -maddrqual=ignore -I"FrameworkHeaders" -I"ProjectHeaders" -Wa,-a -msummary=-psect,-class,+mem,-hex,+file  -ginhx032 -Wl,--data-init -mkeep-startup -mno-osccal -mno-resetbits -mno-save-resetbits -mno-download -mno-stackcall -std=c99 -gdwarf-3 -mstack=hybrid:auto:auto     $(COMPARISON_BUILD) -Wl,--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml -o dist/${CND_CONF}/${IMAGE_TYPE}/TAProject.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}     
	
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
