/****************************************************************************
 Module
   ReceiveFSM.c

 Revision
   1.0.1

 Description
   This is a template file for implementing flat state machines under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 05/27/20       PMA      Initial version of file, adding input capture
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunTemplateSM()
 10/23/11 18:20 jec      began conversion from SMTemplate.c (02/20/07 rev)
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ReceiveFSM.h"
#include "PacketInterpretation.h"

/* Debug header */
//#define DEBUG
/*----------------------------- Module Defines ----------------------------*/
#define DB00    1000
#define DB01    800
#define DB10    600
#define DB11    400
#define THRESH   25

#define LOWORD(l) (*((unsigned int *)(&l)))
#define HIWORD(l) (*(((unsigned int *)(&l))+1))

#define DB00_LTHRES   DB00 - THRESH
#define DB00_UTHRES   DB00 + THRESH
#define DB01_LTHRES   DB01 - THRESH
#define DB01_UTHRES   DB01 + THRESH
#define DB10_LTHRES   DB10 - THRESH
#define DB10_UTHRES   DB10 + THRESH
#define DB11_LTHRES   DB11 - THRESH
#define DB11_UTHRES   DB11 + THRESH
/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/
static void initRXHardware (void);
static uint8_t period2dibit (uint16_t period);
/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file
static TemplateState_t CurrentState;
static uint8_t ChecksumVal = 0; 
// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t MyPriority;
static bool InputLine;
static bool LastInputLine;

static uint8_t symbolCnt;
static volatile uint16_t period;
static uint16_t LastCapVal;
//static uint16_t RxBuffer[8];
static uint32_t dibitBuffer;
static uint8_t  dibitCounter; 
static uint32_t receivedMessage;
/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitReceiveFSM

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition and does any
     other required initialization for this state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
bool InitReceiveFSM(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  // put us into the Initial PseudoState
  CurrentState = InitPState;
  ChecksumVal = 0;
  initRXHardware();
  //printf("Hardware ready!");
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostReceiveFSM

 Parameters
     EF_Event_t ThisEvent , the event to post to the queue

 Returns
     boolean False if the Enqueue operation failed, True otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostReceiveFSM(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunReceiveFSM

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes
   uses nested switch/case to implement the machine.
 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunReceiveFSM(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  switch (CurrentState)
  {
    case InitPState:        // If current state is initial Psedudo State
    {
      if (ThisEvent.EventType == ES_INIT)    // only respond to ES_Init
      {
        // this is where you would put any actions associated with the
        // transition from the initial pseudo-state into the actual
        // initial state
        
        // now put the machine into the actual initial state
        CurrentState = Waiting;
        ChecksumVal = 0;
      }
    }
    break;

    case Waiting:        // If current state is state one
    {
        if (ThisEvent.EventType == TONE_DETECTED)
        {
            dibitCounter = 0;
            //Start timer 1 and CC
            T1CONbits.TMR1ON = 1;
            CCP1CONbits.CCP1EN = 1;
            //Initialize timer for half a bit time (50 ms)
            ES_Timer_InitTimer(HALF_DIBIT_TIMER, 5);
            CurrentState = Listening;  
        }
    }
    break;
    case Listening:
    {
        if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == HALF_DIBIT_TIMER){
            //Save local period
            uint16_t per = period;
            uint8_t dibit = period2dibit(per);
            if (dibit == 3) {
                //Set checksum to 0
                ChecksumVal = 0;
                ES_Timer_InitTimer(DIBIT_TIMER, 10);
            }
            else {
                //Bad dibit received, return to waiting state
                T1CONbits.TMR1ON = 0;
                CCP1CONbits.CCP1EN = 0;
                CurrentState = Waiting;
            }
        }
        else if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == DIBIT_TIMER){
            //Decode the dibit
            uint16_t per = period;
            uint8_t dibit = period2dibit(per);
              
            //Save the incoming dibit to the buffer
            if (dibit < 4) {
                //Received correct dibit
                ChecksumVal += dibit;
                dibitBuffer += dibit;
                dibitBuffer <<= 2L;
                #ifdef DEBUG
                    printf("Dibit: %d , Accum sum: %d \n\r",dibit,dibitBuffer);
                #endif  
                if (dibitCounter >= 9) {
                    //Change state to checksum
                    CurrentState = Checksum;
                }
                dibitCounter ++;
                ES_Timer_InitTimer(DIBIT_TIMER, 10);    
            }
            else {
                //Bad dibit received, return to waiting state
                T1CONbits.TMR1ON = 0;
                CCP1CONbits.CCP1EN = 0;
                CurrentState = Waiting;
            }
        }    
    }
    break;
    case Checksum:
    {
        if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == DIBIT_TIMER) {
            //Decode the dibit
            uint16_t per = period;
            T1CONbits.TMR1ON = 0;
            CCP1CONbits.CCP1EN = 0;
            uint8_t Checksum = period2dibit(per);
            ChecksumVal = 3 - (ChecksumVal % 4);
            #ifdef DEBUG
            
                printf("Chksum: %d Calc Chksum: %d",Checksum, ChecksumVal);
            #endif    
            if (ChecksumVal % 4 == Checksum) {
                dibitBuffer >>= 2L;
                receivedMessage = dibitBuffer;
                dibitBuffer = 0;
                ES_Event_t ThisEvent;
                ThisEvent.EventType  = NEW_MESSAGE;
                PostPacketInterpretation(ThisEvent);
                
            #ifdef DEBUG
                uint16_t low = LOWORD(receivedMessage);
                uint16_t hiw = HIWORD(receivedMessage); 
                printf("Received message!  %d  %d\n\r",hiw,low);
            #endif 
            }
            else {
                dibitBuffer = 0;
                receivedMessage = 0;
            #ifdef DEBUG
                printf("Bad checksum\n\r");
            #endif
            }
            //Return to wait for next checksum
            
            ES_Timer_InitTimer(DIBIT_TIMER, 20); 
            CurrentState = QuietTime;
        }
    }
    break;
    case QuietTime: 
    {
        if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == DIBIT_TIMER) {  
            CurrentState = Waiting;
        }
    }
    
  }  
  // end switch on Current State
  return ReturnEvent;
}


void RXISR (void) {
    PORTCbits.RC1 = 1;
    static uint16_t CurCapVal;  //static for speed
    static uint16_t periodTemp; // holder for calculation
    //clear flag
    PIR6bits.CCP1IF = 0;
    // Clear the quiet line timer
    T0CON0bits.T0EN = 0;
    TMR0L = 0;
    T0CON0bits.T0EN = 1;
    //Calculate the period (new-last)
    CurCapVal = CCPR1;
    periodTemp = CurCapVal-LastCapVal;
    // Check period is within expected limits
    if((periodTemp < DB00_UTHRES ) && (periodTemp > DB11_LTHRES))
    {
        period = periodTemp;
    }
    //update last value
    LastCapVal = CurCapVal;
    PORTCbits.RC1 = 0;
}

/****************************************************************************
 Function
     QueryTemplateSM

 Parameters
     None

 Returns
     TemplateState_t The current state of the Template state machine

 Description
     returns the current state of the Template state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:21
****************************************************************************/
TemplateState_t QueryReceiveFSM(void)
{
  return CurrentState;
}

uint32_t GetLatestMessage(void) {
    return receivedMessage;
}

/***************************************************************************
 Event Checker
 ***************************************************************************/
bool CheckForLine(void)
{   
    bool ReturnVal = false;
    //Read the current line measurement
    InputLine = PORTCbits.RC2;
    
    if (InputLine != LastInputLine)        // new key waiting?
    {
      ES_Event_t ThisEvent;
      ThisEvent.EventType   = TONE_DETECTED;
      ES_PostAll(ThisEvent);
      ReturnVal = true;
    }
    LastInputLine = InputLine;
    return ReturnVal;
}


/***************************************************************************
 private functions
 ***************************************************************************/
static void initRXHardware (void) {
    //Set up RC4 as input
    TRISCbits.TRISC2 = 1;
    //Disable analog for RC2
    ANSELCbits.ANSC2 = 0;
    ANSELCbits.ANSC1 = 0;
    //Setup RC1 as debug line
    TRISCbits.TRISC1 = 0;
    //Set line as low
    PORTCbits.RC1 = 0;
    //Save the value of the input line here
    LastInputLine = PORTCbits.RC2;
    
    //Initialize Timer 1
    T1CONbits.ON = 0; //Verify that the timer is off
    //Set source to be fosc/4
    T1CLK |= 0x1;
    //Set prescaler to be 1:8
    T1CONbits.CKPS = 0x3;
    
    // Setup TMR0 as a quite line detect timer
    // Ensure timer is off
    T0CON0bits.T0EN = 0;
    // Set to 8 bit mode
    T0CON0bits.T016BIT = 0;
    // Set prescale to 64:1 and clock to LFINTOSC
    T0CON1 = 0b10000110;
    // Set match value to half a symbol time
    TMR0H = 120;
    TMR0L = 0;
    // Clear int flag
    PIR0bits.TMR0IF = 0;
    
    // Setup CCP1
    CCP1CONbits.CCP1EN = 0;
    // Set mode to input capture
    CCP1CON = (CCP1CON & 0xf0)| 0b0101;
    // Set RC2 to be the input pin
    CCP1CAP = 0x00;
    // Clear any pending interrupt
    PIR6bits.CCP1IF = 0;
    // Enable the input capture interrupt
    PIE6bits.CCP1IE = 1;
}

static uint8_t period2dibit (uint16_t period) {
    //Function that grabs the corresponding dibit from the incoming period
    if (period >= DB00_LTHRES && period <= DB00_UTHRES){
        return 0;
    } else if (period >= DB01_LTHRES && period <= DB01_UTHRES){
        return 1;
    } else if (period >= DB10_LTHRES && period <= DB10_UTHRES){
        return 2;
    } else if (period >= DB11_LTHRES && period <= DB11_UTHRES){
        return 3;
    }
    return 4;
}