#include <QApplication>
#include "game.h"
#include "startmenu.h"

Game * game;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    game = new Game();
    StartMenu * startmenu = new StartMenu();

    return a.exec();
}
