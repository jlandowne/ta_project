/*
 * file: sub.cpp
 * ----------------
 * This class creates our sub
 */
#include "sub.h"
#include "coordinates.h"
#include "game.h"
#include "definitions.h"

#include <QObject>
#include <QGraphicsScene>
#include <QKeyEvent>
#include <QDebug>

#define UP 90
#define RIGHT_UP 150
#define RIGHT_DOWN 210
#define DOWN 270
#define LEFT_DOWN 330
#define LEFT_UP 30


extern Game * game;

Sub::Sub(QGraphicsItem *parent): QGraphicsPixmapItem(parent)
{
    QPixmap p = QPixmap(":/images/sub2.png");
    setPixmap(p.scaledToHeight(7*ScreenScale*15));
    setTransformOriginPoint(this->boundingRect().width()/2, this->boundingRect().height()/2);
    setRotation(90);
    Heading = 90;
    X_Coord = 7;
    Y_Coord = 0;
}

void Sub::move()
{
    if(Heading == UP) {
        Y_Coord++;
    } else if(Heading == DOWN) {
        Y_Coord--;
    } else if(Heading == LEFT_UP) {
        X_Coord--;
        Y_Coord++;
    } else if(Heading == RIGHT_UP) {
        X_Coord++;
    } else if(Heading == LEFT_DOWN) {
        X_Coord--;
    } else if(Heading == RIGHT_DOWN) {
        X_Coord++;
        Y_Coord--;
    }
    int Xpixels = game->coord->X_Pixels(X_Coord,Y_Coord);
    int Ypixels = game->coord->Y_Pixels(X_Coord,Y_Coord);
    this->setPos(Xpixels,Ypixels);
}

void Sub::turnLeft()
{
    Heading -= 60;
    if(Heading < 30) Heading += 360;
    this->setTransformOriginPoint(this->boundingRect().width()/2, this->boundingRect().height()/2);
    this->setRotation(Heading);
    qDebug()<<"Got left arrow";

}

void Sub::turnRight()
{
    Heading += 60;
    Heading = Heading % 360;
    this->setTransformOriginPoint(this->boundingRect().width()/2, this->boundingRect().height()/2);
    this->setRotation(Heading);
    qDebug()<<"Got right arrow";
    //CurrentHeading++;
}

/* This was for testing */
void Sub::keyPressEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_Left) {
        this->turnLeft();
    } else if(event->key() == Qt::Key_Right) {
        this->turnRight();
    } else if(event->key() == Qt::Key_Up) {
        this->move();
    }
    qDebug()<<"Getting a key press";
}

