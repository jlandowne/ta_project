/****************************************************************************
 Module
   PacketInterpretation.c

 Revision
   1.0.1

 Description
   This service interprets packets received by ReceiveFSM according to the
   communication protocol specified by the 2020 Comm Committee, currently up to
   Revision 6.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 05/27/2020     bibit   began file
 01/16/12 09:58 jec     began conversion from TemplateFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
// This module
#include "PacketInterpretation.h"

// debugging printf()
#include "dbprintf.h"

// Event & Services Framework
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"
#include "ES_Port.h"

// Other modules
#include "SerialService.h"
#include "ReceiveFSM.h"


//#define DEBUGPRINT(x) printf(x)
/*----------------------------- Module Defines ----------------------------*/
/*
   23 22 21 20 19 18 17 16 15 14 13 12 11 10  9  8  7  6  5  4  3  2  1  0
    0  0  0  0 T1 T2  *  ADDRESS STAT. ACTIV/HEAD. DATA-DATA-DATA-DATA-DATA
   
  ...where:
    - T1 and T2 [bits 18-19] are the two LSBs of the current turn
    - * [bit 17] is set if the packet is intended for intra-team communication
    - ADDRESS [bits 14-16] are the team address
    - STAT. [bits 12-13] are the station address
    - ACTIV/HEAD [bits 8-11] are the activity or heading of the sub
    - DATA [bits 0-7] are the data of the packet
  */

// The following masks are to be applied to NextPacket.
#define TURN_MASK               (BIT18HI | BIT19HI)
#define INTRA_MASK              (BIT17HI)
#define SOURCE_SUB_MASK         (BIT14HI | BIT15HI | BIT16HI)
#define SOURCE_STATION_MASK     (BIT12HI | BIT13HI)
#define ACTIVITY_MASK           (BIT8HI | BIT9HI | BIT10HI | BIT11HI)
#define HEADING_MASK            (BIT8HI | BIT9HI | BIT10HI)
#define DATA_MASK               (BIT0HI | BIT1HI | BIT2HI | BIT3HI | \
                                 BIT4HI | BIT5HI | BIT6HI | BIT7HI)

// The following shifts are to be applied after masking NextPacket to get the
// information of interest into the least significant bits.
#define TURN_SHIFT              18L
#define INTRA_SHIFT             17L
#define SOURCE_SUB_SHIFT        14L
#define SOURCE_STATION_SHIFT    12L
#define ACTIVITY_SHIFT          8L
#define HEADING_SHIFT           8L
#define DATA_SHIFT              0L

// The following are any identified activities, shifted such that the activity
// and heading are in the four least significant bits.
#define ACTIVITY_RIP            0b0000
#define ACTIVITY_NEXT_TURN      0b0001
#define ACTIVITY_NO_ACTION      0b0010
#define ACTIVITY_SYNC           0b0011
#define ACTIVITY_LAUNCHED       0b0100
#define ACTIVITY_DETONATE       0b0101
#define ACTIVITY_POS_REQUEST    0b0110
#define ACTIVITY_HIT            0b0111
#define ACTIVITY_ECHO           0b1000
#define ACTIVITY_PING_OMNI      0b1111

// The following are the known headings, shifted to the three least significant
// bits.
#define HEADING_UP          001b
#define HEADING_RIGHT_UP    010b
#define HEADING_RIGHT_DOWN  011b
#define HEADING_DOWN        100b
#define HEADING_LEFT_DOWN   101b
#define HEADING_LEFT_UP     110b


// these times assume a 10.000mS/tick timing
#define LOWORD(l) (*((unsigned int *)(&l)))
#define HIWORD(l) (*(((unsigned int *)(&l))+1))

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/
static void InterpretPacket(void);

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
static uint32_t NextPacket;

static uint8_t SourceSub;
static uint8_t SourceStation;
static uint8_t Activity;
static uint8_t Heading;
static uint8_t Turn;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitPacketInterpretation

 Parameters
     uint8_t : the priority of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any other required initialization for
     this service
 
 Notes

 Author
     Bibit Bianchini, 5/27/2020
****************************************************************************/
bool InitPacketInterpretation(uint8_t Priority)
{
  ES_Event_t ThisEvent;
  MyPriority = Priority;

  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostPacketInterpretation

 Parameters
     ES_Event_t ThisEvent, the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     Bibit Bianchini, 5/27/2020
****************************************************************************/
REENTRANT bool PostPacketInterpretation(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunPacketInterpretation

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here

 Notes

 Author
     Bibit Bianchini, 5/27/2020
****************************************************************************/
ES_Event_t RunPacketInterpretation(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

#ifdef _INCLUDE_BYTE_DEBUG_
  _HW_ByteDebug_SetValueWithStrobe( ENTER_RUN );
#endif
  
  switch (ThisEvent.EventType)
  {
    case ES_INIT:
    {
      
    }
    break;
    
    case NEW_MESSAGE:
    {
        // query the packet
        NextPacket = GetLatestMessage();
        
        // interpret the packet
        InterpretPacket();
        
    }
    break;
    
    default:
    {}
     break;
  }

  return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/
/****************************************************************************
 Function
   InterpretPacket

 Parameters
   none

 Returns
   none

 Description
   This function breaks down the 24-bit NextPacket variable and identifies
   what is happening.  This function posts relevant events to the Serial
   Service.

 Notes
   The NextPacket variable looks as follows:
   
   23 22 21 20 19 18 17 16 15 14 13 12 11 10  9  8  7  6  5  4  3  2  1  0
    0  0  0  0 T1 T2  *  ADDRESS STAT. ACTIV/HEAD. DATA-DATA-DATA-DATA-DATA
   
  ...where:
    - T1 and T2 [bits 18-19] are the two LSBs of the current turn
    - * [bit 17] is set if the packet is intended for intra-team communication
    - ADDRESS [bits 14-16] are the team address
    - STAT. [bits 12-13] are the station address
    - ACTIV/HEAD [bits 8-11] are the activity or heading of the sub
    - DATA [bits 0-7] are the data of the packet

 Author
     Bibit Bianchini, 5/27/2020
****************************************************************************/
static void InterpretPacket(void)
{    
    //DEBUGPRINT("PI %d %d  ",HIWORD(NextPacket), LOWORD(NextPacket)); 
    // 1. determine which sub was the source
    SourceSub = (uint32_t)(NextPacket & SOURCE_SUB_MASK) >> (uint32_t) SOURCE_SUB_SHIFT;
    
    // 2. determine which station was the source
    SourceStation = (uint32_t) (NextPacket & SOURCE_STATION_MASK) >> (uint32_t) SOURCE_STATION_SHIFT;
    
    // 3. isolate the activity/heading
    Activity = (uint32_t) (NextPacket & ACTIVITY_MASK) >> (uint32_t) ACTIVITY_SHIFT;
    
    // 4. mask the following turn
    Turn = (uint32_t) (NextPacket & TURN_MASK) >> (uint32_t) TURN_SHIFT;
    //DEBUGPRINT("Activity %d, Source %d \n\r",Activity, SourceSub);
    switch (Activity)
    {
        // 4. identify RIP
        case ACTIVITY_RIP:
        {
            // RIP
            
            // post to the serial service that the sub is dead
            ES_Event_t PostEvent;
            PostEvent.EventType = DEAD_SUB;
            PostEvent.EventParam = SourceSub;
            PostSerialService(PostEvent);
            PostPacketInterpretation(PostEvent);
        }
        break;

        // 5. identify SYNC
        case ACTIVITY_SYNC:
        {
            // SYNC
            
        }
        break;

        // 6. identify NO_ACTION
        case ACTIVITY_NO_ACTION:
        {
            // NO_ACTION
        }
        break;

        // 7. identify NEXT_TURN
        case ACTIVITY_NEXT_TURN:
        {
            // NEXT_TURN
            ES_Event_t PostEvent;
            PostEvent.EventType = NEXT_TURN;
            PostEvent.EventParam = (NextPacket & DATA_MASK);
            PostSerialService(PostEvent);
        }
        break;

        // 8. identify LAUNCHED
        case ACTIVITY_LAUNCHED:
        {
            // LAUNCHED
        }
        break;

        // 9. identify DETONATE
        case ACTIVITY_DETONATE:
        {
            // DETONATE
            
            // grab the detonation location encoded in the data
            uint16_t DetonationLocationAndSource;   // uint16 because will need
                                                    // to fit the sub address in
                                                    // upper bits too
            DetonationLocationAndSource = (NextPacket & DATA_MASK) >> DATA_SHIFT;
            
            // put the source sub into bits 8 through 10
            DetonationLocationAndSource |= (SourceSub << 8);
                
            // post to the serial service the detonation location
            ES_Event_t PostEvent;
            PostEvent.EventType = DETONATION;
            PostEvent.EventParam = DetonationLocationAndSource;
            PostSerialService(PostEvent);
        }
        break;

        // 10. identify POS_REQUEST
        case ACTIVITY_POS_REQUEST:
        {
            // POS_REQUEST
        }
        break;

        // 11. identify HIT
        case ACTIVITY_HIT:
        {
            // HIT
            // grab the hit sub's location encoded in the data
            uint16_t HitLocationAndSource;      // uint16 because will need to
                                                // fit the sub address in upper
                                                // bits too
            HitLocationAndSource = (NextPacket & DATA_MASK) >> DATA_SHIFT;
            
            // put the hit sub's team number into bits 8 through 10
            HitLocationAndSource |= (SourceSub << 8);
                
            // post to the serial service the hit team and their location
            ES_Event_t PostEvent;
            PostEvent.EventType = SUB_HIT;
            PostEvent.EventParam = HitLocationAndSource;
            PostSerialService(PostEvent);
        }
        break;

        // 12. identify ECHO
        case ACTIVITY_ECHO:
        {
            // ECHO
        }
        break;

        // 13. identify PING_OMNI
        case ACTIVITY_PING_OMNI:
        {
            // PING_OMNI
            
            // grab the pinging sub's location encoded in the data
            uint16_t OmniPingLocationAndSource; // uint16 because will need to
                                                // fit the sub address in upper
                                                // bits too
            OmniPingLocationAndSource = (NextPacket & DATA_MASK) >> DATA_SHIFT;
            
            // put the pinging sub's team number into bits 8 through 10
            OmniPingLocationAndSource |= (SourceSub << 8);
                
            // post to the serial service the pinging team and their location
            ES_Event_t PostEvent;
            PostEvent.EventType = PING_OMNI;
            PostEvent.EventParam = OmniPingLocationAndSource;
            PostSerialService(PostEvent);
        }
        break;
        
        default:
        {
            // in this case, the event is either POS_UPDATE or PING_DIRECTED,
            // and the heading is contained in the packet
            Heading = (NextPacket & HEADING_MASK) >> HEADING_SHIFT;
            
            // 14. identify POS_UPDATE
            if (NextPacket & INTRA_MASK)
            {
                // POS_UPDATE
                
                // grab the location encoded in the data
                uint16_t LocationAndAddress;    // uint16 because will need to
                                                // fit the sub address in upper 
                                                // bits too
                LocationAndAddress = (NextPacket & DATA_MASK) >> DATA_SHIFT;
                
                // put the heading into bits 8 through 10
                LocationAndAddress |= (Heading << 8);
                
                // put the source sub into bits 11 through 13
                LocationAndAddress |= (SourceSub << 11);
                
                // post to the serial service the updated sub location
                ES_Event_t PostEvent;
                PostEvent.EventType = LOCATION_UPDATE;
                PostEvent.EventParam = LocationAndAddress;
                PostSerialService(PostEvent);
            }
            
            // 15. identify PING_DIRECTED
            else
            {
                // PING_DIRECTED
            
                // grab the pinging sub's location encoded in the data
                uint16_t DirectedPingLocationAndSource; // uint16 because will
                                                        // need to fit the sub
                                                        // address in upper bits
                                                        // too
                DirectedPingLocationAndSource = (NextPacket & DATA_MASK) >> DATA_SHIFT;
                
                // put the heading into bits 8 through 10
                DirectedPingLocationAndSource |= (Heading << 8);
                
                // put the source sub into bits 11 through 13
                DirectedPingLocationAndSource |= (SourceSub << 11);

                // post to the serial service the pinging team and location
                ES_Event_t PostEvent;
                PostEvent.EventType = PING_DIRECTED;
                PostEvent.EventParam = DirectedPingLocationAndSource;
                PostSerialService(PostEvent);
            }
            
        }
        break;

    }
    
}

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

