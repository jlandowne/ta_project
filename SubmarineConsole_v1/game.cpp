/*
 * file: game.cpp
 * ------------------
 * This file handles all game play set up and signal response to buttons
 */
#include "game.h"
#include "sub.h"
#include "button.h"
#include "enemy.h"
#include "coordinates.h"
#include "definitions.h"
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QTimer>
#include <QObject>
#include <QBrush>
#include <QImage>
#include <QDebug>
#include <QSerialPort>
#include <QTimer>
#include <QVector>

#define TIMER_LENGTH 2000
#define PACKET_LENGTH 6
//#define PORT_TEST

Game::Game(QWidget * parent)
{
    /* Set up scene */
    //Set up background
    scene = new QGraphicsScene();
    scene->setSceneRect(0,0,ScreenWidth*ScreenScale,ScreenHeight*ScreenScale);
    setBackgroundBrush(QBrush(QImage(":/images/bg.png")));
    setScene(scene);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setFixedSize(ScreenWidth*ScreenScale,ScreenHeight*ScreenScale);

    //Set up coordinates
    coord = new Coordinates();

    /* Set up controls for the game */
    //Add control box titles
    QGraphicsTextItem* titleText = new QGraphicsTextItem(QString("GAME IN PLAY"));
    QFont titleFont("comic sans", 50);
    titleText->setDefaultTextColor(Qt::white);

    titleText->setFont(titleFont);
    int txPos = ScreenWidth*ScreenScale - ScreenWidth*ScreenScale/5;
    int tyPos = 10*ScreenScale;
    titleText->setPos(txPos-titleText->boundingRect().width()/2, tyPos);
    scene->addItem(titleText);

    //Hit "Button"
    hitButton = new Button(QString("HIT"));
    hitButton->setPos(txPos - hitButton->boundingRect().width()/2, CENTER_Y - ITEM_OFFSET/2 - hitButton->boundingRect().height()/2);
    scene->addItem(hitButton);

    hitBlinkTimer = new QTimer();
    hitBlinkTimer->setSingleShot(true);
    QTimer::connect(hitBlinkTimer, SIGNAL(timeout()), this, SLOT(blinkHit()));

    //Detonation "Button"
    detonationButton = new Button(QString("DETONATION"));
    detonationButton->setPos(txPos - detonationButton->boundingRect().width()/2, CENTER_Y + ITEM_OFFSET/2 - detonationButton->boundingRect().height()/2);
    scene->addItem(detonationButton);

    detonationBlinkTimer = new QTimer();
    detonationBlinkTimer->setSingleShot(true);
    QTimer::connect(detonationBlinkTimer, SIGNAL(timeout()), this, SLOT(blinkDetonation()));

    //Ping "Button"
    pingButton = new Button(QString("PING"));
    pingButton->setPos(txPos - pingButton->boundingRect().width()/2, CENTER_Y - pingButton->boundingRect().height()/2);
    scene->addItem(pingButton);

    pingBlinkTimer = new QTimer();
    pingBlinkTimer->setSingleShot(true);
    QTimer::connect(pingBlinkTimer, SIGNAL(timeout()), this, SLOT(blinkPing()));

    //Turn Number
    TurnText = new QGraphicsTextItem(QString("Turn: 0"));
    TurnText->setDefaultTextColor(Qt::white);
    TurnText->setFont(titleFont);
    TurnText->setPos(txPos - TurnText->boundingRect().width()/2, tyPos + ITEM_OFFSET/2 + TurnText->boundingRect().height()/2);
    scene->addItem(TurnText);

    //Explosion after Detonation
    explosion = new Explosion();

    //Set up enemies, not shown until we get a position update
    for (int i = 0; i < numTeams; i++) {
        Enemy * en = new Enemy();
        teams.append(en);
        en->setEnemyNum(i);
        scene->addItem(en);
    }

    //Set up serial port
    serial = new QSerialPort(this);
    serial->setPortName(DEFAULT_SERIAL_PORT);
    serial->setBaudRate(QSerialPort::Baud9600);
    serial->setDataBits(QSerialPort::Data8);
    serial->setParity(QSerialPort::NoParity);
    serial->setStopBits(QSerialPort::OneStop);
    serial->setFlowControl(QSerialPort::NoFlowControl);
    serial->open(QIODevice::ReadWrite);
    connect(serial, SIGNAL(readyRead()),this,SLOT(serialReceived()));
    if(serial->isOpen()) {
        qDebug() << "Port Success!";
#ifdef PORT_TEST
        char data[] = {'H',1};
        serial->write(data);
#endif
    } else {
        qDebug() << "PORT FAILURE ON INIT, consider changing default port in definitions.h";
    }
}

Game::~Game()
{
    serial->close();
}

/* This function handles receiving information from the serial port. It
 * requires a code "GS" in order to start processing data. After that,
 * everything is space-delimited.
 */
void Game::serialReceived()
{
    if(serial->bytesAvailable() < PACKET_LENGTH) {
        if (numBadReads > 1) {
            serial->readAll();
            numBadReads = 0;
        }
        return;
    }
    QByteArray ba;
    ba = serial->read(PACKET_LENGTH);
    int len = ba.size();
    char msgType = ba.front();
    Enemy * thisTeam;
    qDebug() << ba;
    switch(msgType) {
    case 'H': { //Hit
        if (len < PACKET_LENGTH) return;
        int teamNum = QString(ba.at(1)).toInt();
        thisTeam = teams[teamNum];
        gotHit();
        thisTeam->updateEnemy(thisTeam->curX, thisTeam->curY, DEAD);
        numBadReads = 0;
        return;
    }
        //WORKS
    case 'D': { //Detonation
        if (len < PACKET_LENGTH) return;
        int locX = (QString(ba.at(1)) + QString(ba.at(2))).toInt();
        int locY = (QString(ba.at(3)) + QString(ba.at(4))).toInt();
        qDebug() << locX;
        qDebug() << locY;
        explosion->set(locX, locY);
        gotDetonation();
        numBadReads = 0;
        return;
    }
    case 'P': { //Ping
        if(len < PACKET_LENGTH) return;
        gotPing();
        qDebug() << "PIIIIING";
        numBadReads = 0;
        return;
    }
    case 'L': { //Location update
        if (len < PACKET_LENGTH) return;
        int teamNum = QString(ba.at(1)).toInt();
        thisTeam = teams.value(teamNum);
        int locX = (QString(ba.at(2)) + QString(ba.at(3))).toInt();
        int locY = (QString(ba.at(4)) + QString(ba.at(5))).toInt();
        qDebug() << teamNum;
        qDebug() << locX;
        qDebug() << locY;
        qDebug() << (21 - locX - locY);

        thisTeam->updateEnemy(locX, locY, ALIVE);
        numBadReads = 0;
        return;
    }
    case 'N': { //Next turn
        if (len < PACKET_LENGTH) return;
        int turn = (QString(ba.at(1)) + QString(ba.at(2)) + QString(ba.at(3))).toInt();
        qDebug() << turn;
        TurnText->setPlainText(QString("Turn: ") + QString::number(turn));
        numBadReads = 0;
        return;
    }
    default:
        serial->readAll();
        numBadReads++;
        return;
    }

}

void Game::blinkDetonation() {
    detonationButton->setColor(Qt::gray);
    explosion->hide();
    scene->update();
}

void Game::blinkHit() {
    hitButton->setColor(Qt::gray);
}

void Game::blinkPing() {
    pingButton->setColor(Qt::gray);
}

void Game::gotHit() {
    hitButton->setColor(Qt::red);
    hitBlinkTimer->start(TIMER_LENGTH);
}

void Game::gotPing() {
    pingButton->setColor(Qt::red);
    pingBlinkTimer->start(TIMER_LENGTH);
}

void Game::gotDetonation() {
    explosion->show();
    scene->update();
    detonationButton->setColor(Qt::red);
    detonationBlinkTimer->start(TIMER_LENGTH);
}
