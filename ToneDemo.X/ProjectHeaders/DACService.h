/****************************************************************************

  Header file for template service
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef DACServ_H
#define DACServ_H

#include "ES_Types.h"

// Public Function Prototypes

bool InitDACService(uint8_t Priority);
bool PostDACService(ES_Event_t ThisEvent);
ES_Event_t RunDACService(ES_Event_t ThisEvent);

// Event Checker
bool CheckForADCDone(void);

// tone ISR
void ToneGenISR(void);
// decode ISR
void ToneDecodeISR(void);

#endif /* DACServ_H */

