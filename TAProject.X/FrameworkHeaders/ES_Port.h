/****************************************************************************
 Module
     ES_Port.h
 Description
     header file to collect all of the hardware/platform dependent info for
     a particular port of the ES framework
 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 10/26/17 18:39 jec     moves definition of ALL_BITS to here
 10/14/15 21:50 jec     added prototype for ES_Timer_GetTime
 01/18/15 13:24 jec     clean up and adapt to use TI driver lib functions
                        for implementing EnterCritical & ExitCritical
 03/13/14		joa		      Updated files to use with Cortex M4 processor core.
                        Specifically, this was tested on a TI TM4C123G mcu.
 08/13/13 12:12 jec     moved the timer rate constants from ES_Timers.h here
 08/05/13 14:24 jec     new version replacing port.h and adding define to
                        capture the C99 compliant behavior of the compiler
*****************************************************************************/
#ifndef ES_PORT_H
#define ES_PORT_H

// pull in the hardware header files that we need
#include <xc.h>

#include <stdio.h>
#include <stdint.h>
#include "bitdefs.h"        /* generic bit defs (BIT0HI, BIT0LO,...) */
#include "Bin_Const.h"      /* macros to specify binary constants in C */
#include "ES_Types.h"

// macro to control the use of C99 data types (or simulations in case you don't
// have a C99 compiler).
#define COMPILER_IS_C99

// The macro 'ES_FLASH' is needed on some compilers to allocate const data to
// ROM. The macro must be defined, even if it evaluates to nothing.
// for the 'C32 & E128 this is not needed so it evaluates to nothing
#define ES_FLASH

// the macro 'ES_READ_FLASH_BYTE' is needed on some Harvard machines to generate
// code to read a byte from the program space (Flash)
// The macro must be defined, even if it evaluates to nothing.
// for the 'C32 & E128 we don't need special instructions so it evaluates to a
// simple reference to the variable
#define ES_READ_FLASH_BYTE(_flash_var_) (_flash_var_)

// The macro 'REENTRANT' is needed on some compilers to get them to create
// reentrant code. In order to post from an ISR, we need for ES_PostToService,
// ES_EnqueueFIFO, and any service post function that will be called from an
// ISR to be reentrant.
#define REENTRANT __reentrant

// these macros provide the wrappers for critical regions, where ints will be off
// but the state of the interrupt enable prior to entry will be restored.
// allocation of temp var for saving interrupt enable status should be defined
// in ES_Port.c


extern uint8_t _INTCON_temp;

// for the PIC, at this time, we can not post from within interrupts so keep 
// this definition commented. if you ever get posting from within a int working
// then uncomment it.
//#define POST_FROM_INTS

#define EnterCritical() { _INTCON_temp = INTCON; GIE=0;}
#define ExitCritical() { INTCON = _INTCON_temp; }


/* Rate constants for programming the SysTick Period to generate tick interrupts.
   These assume that we are using the NCO to generate the systick and that its
   clock source is the 31.25kHz MFINTOSC
 */
typedef enum
{
  ES_Timer_RATE_OFF   = (0),
  ES_Timer_RATE_5mS   = 6711,  /* 0.0049999153628371330651169721353 */  
  ES_Timer_RATE_10mS  = 3355,  /* 0.01000132101341281669150521609538 */
  ES_Timer_RATE_20mS  = 1678,  /* 0.01999668176400476758045292014303 */
  ES_Timer_RATE_50mS  = 671    /* 0.0500066050670640834575260804769 */
}TimerRate_t;

// map the generic functions for testing the serial port to actual functions
// for this platform. If the C compiler does not provide functions to test
// and retrieve serial characters, you should write them in ES_Port.c
//#define IsNewKeyReady() (kbhit() != 0)
#define IsNewKeyReady() (RC1IF)
#define GetNewKey() getchar()

// this function connects printf & puts to the serial port on the PIC
void putch(char data);

bool kbhit(void);                // is a charcter ready on the EUSART?

// prototypes for the hardware specific routines
void _HW_PIC15356Init(void);
void _HW_Timer_Init(const TimerRate_t Rate);
bool _HW_Process_Pending_Ints(void);
uint16_t _HW_GetTickCount(void);
void _HW_ConsoleInit(void);
void _HW_SysTickIntHandler(void);

// and the one Framework function that we define here
uint16_t ES_Timer_GetTime(void);

#endif
